<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => '/'], function () {
	Route::get('/', ['as' => 'homePage', 'uses' => 'WebController@index']);

	Route::resource('/kebutuhan-diklat', 'RequirementController');
	Route::get('/get-kebutuhan-diklat', ['as' => 'getKebutuhan', 'uses' => 'RequirementController@get']);

	Route::resource('/agenda-diklat', 'ScheduleController');
	Route::get('/get-agenda-diklat', ['as' => 'getAgenda', 'uses' => 'ScheduleController@get']);
	Route::post('/post-agenda-diklat/{id}', ['as' => 'postAgenda', 'uses' => 'ScheduleController@update']);
	
	Route::get('/contact', ['as' => 'contact', 'uses' => 'WebController@contact']);

	Route::get('/details/{slug}', ['as' => 'detailsPage', 'uses' => 'WebController@contact'])->where('slug', '[\w\d\-\_]+');

	Route::resource('news', 'NewsController');
	Route::post('/post-news/{id}', ['as' => 'postNews', 'uses' => 'NewsController@update']);

	Route::resource('hasil-diklat', 'ResultsController');
	Route::get('/get-hasil-diklat', ['as' => 'getHasil', 'uses' => 'ResultsController@get']);
	Route::get('/paper-hasil-diklat/{id}', ['as' => 'getPaper', 'uses' => 'ResultsController@paper']);
	Route::post('/post-paper-diklat/{id}', ['as' => 'postPaper', 'uses' => 'ResultsController@update']);

	Route::resource('user-diklat', 'UserController');
	Route::get('/get-user-diklat', ['as' => 'getUser', 'uses' => 'UserController@get']);


	Route::group(['prefix' => 'dashboard', 'middleware' => ['user'], 'as' => 'dashboard.'], function () {
	// Route::group(['prefix' => 'dashboard', 'middleware' => ['user'], 'as' => 'dashboard.'], function () {
		Route::get('/', ['as' => 'dashboardPage', 'uses' => 'WebController@index']);
	});
});

Auth::routes();
