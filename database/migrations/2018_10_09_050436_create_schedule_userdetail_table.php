<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleUserdetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_userdetail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userdetail_id')->unsigned();
            $table->integer('schedule_id')->unsigned();
            $table->string('paper_title')->nullable();
            $table->string('paper')->nullable();
            $table->foreign('userdetail_id')->references('id')->on('userdetails')->onDelete('cascade');
            $table->foreign('schedule_id')->references('id')->on('schedules')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule_userdetail');
    }
}
