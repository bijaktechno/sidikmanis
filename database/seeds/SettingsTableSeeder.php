<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('settings')->insert([

			'website_title' => 'SIDIKMANIS',
			'logo' => 'logo.png',
			'favicon' => 'favicon.png',
			'about_us' => 'sidikmanis.',
			'copyright' => 'Copyright 2018 <a href="#" target="_blank">SIDIKMANIS</a>, All rights reserved.',
			'email' => 'admin@mail.com',
			'phone' => '08998878778',
			'mobile' => '08998878778',
			'fax' => '12345',
			'address_line_one' => 'House# 83, Road# 16, Sector# 11',
			'address_line_two' => 'Uttara',
			'state' => 'Uttara',
			'city' => 'Purwakarta',
			'zip' => '12345',
			'country' => 'Indonesia',
			'map_iframe' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2642905.2881059386!2d89.27605108245604!3d23.817470325158617!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30adaaed80e18ba7%3A0xf2d28e0c4e1fc6b!2sBangladesh!5e0!3m2!1sen!2sbd!4v1520764767552" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
			'facebook' => 'https://facebook.com/clustercoding',
			'twitter' => 'https://twitter.com/cluster_coding',
			'google_plus' => 'https://plus.google.com/+ClusterCoding',
			'linkedin' => 'https://www.linkedin.com/company/clustercoding/',
			'meta_title' => 'sidikmanis',
			'meta_keywords' => 'sidikmanis',
			'meta_description' => 'sidikmanis.',
			'gallery_meta_title' => 'sidikmanis',
			'gallery_meta_keywords' => 'sidikmanis',
			'gallery_meta_description' => 'sidikmanis.',
        ]);
    }
}
