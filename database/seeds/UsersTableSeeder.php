<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	DB::table('users')->insert([

            'id' => 1,
            'group_id' => 1,
            'username' => 'admin',
            'email' => 'admin@mail.com',
            'password' => bcrypt('secret'),
			'remember_token' => str_random(10),
        ]);
        // factory(App\User::class, 10)->create();
    }
}
