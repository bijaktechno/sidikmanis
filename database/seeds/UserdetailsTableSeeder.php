<?php

use Illuminate\Database\Seeder;

class UserdetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('userdetails')->insert([

            'user_id' => 1,
            'name' => 'admin',
            'nip' => 'admin',
            'gol' => 'admin',
            'position' => 'admin',
            'phone' => '',
			'address' => '',
        ]);
    }
}
