$(document).ready(function(){

	$(".submit-btn").click(function(){

        var idBtn = "#"+this.id;
		var defaultBtn = $(this).html();
        var formData = $(this).closest('form').serialize();
        var act = $(this).closest('form').attr('action');

        $(this).addClass("disabled");
        $(this).html("<i class='fa fa-spinner fa-spin'></i> Loading");


        $.ajax({
            url: act,
            type: 'POST',
            data: formData,
            dataType: "json",
	        success:function(data){
                if(data.status == true){
            		// alert(data.message);

                    $('.alert-success').animate({ top: "0" }, 500).show();
                    $('.alert-success').html(data.message);

                    setTimeout(function () {
                        $(idBtn).removeClass("disabled");
                        $(idBtn).html(defaultBtn);
                        location.reload();
                    }, 2000);
                }
                else{
                    var arr = data;
                    var messages = '';

                    $.each(arr, function(index, value)
                    {
                        if (value.length != 0)
                        {
                            messages += value+"<br>";
                            // $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
                        }
                    });

                    // alert(messages);
                    
                    $('.alert-danger').animate({ top: "0" }, 500).show();
                    $('.alert-danger').html(messages);

                    setTimeout(function(){
                        hideAllMessages();
                        $(idBtn).removeClass("disabled");
                        $(idBtn).html(defaultBtn);
                    }, 4000);

                }
	        },
	        error: function (data, result) {

                console.log(data);

                var arr = data.responseJSON.errors;
                var messages = '';

                $.each(arr, function(index, value)
                {
                    if (value.length != 0)
                    {
                        messages += value+"<br>";
                        // $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
                    }
                });

                $('.alert-danger').animate({ top: "0" }, 500).show();
                $('.alert-danger').html(messages);

                setTimeout(function(){
                    hideAllMessages();
                    $(idBtn).removeClass("disabled");
                    $(idBtn).html(defaultBtn);
                }, 4000);
	        }

        });

		event.preventDefault();

        return false;
		
	});
});



var myMessages = ['alert-success','alert-info','alert-warning','alert-danger']; // define the messages types      


$(document).ready(function(){
     
     hideAllMessages();
     
     $('.alert').click(function(){        
          $(this).animate({top: -$(this).outerHeight()}, 500).show();
      });    
     
});  

function hideAllMessages()
{
    // console.log(myMessages);

     var messagesHeights = new Array(); // this array will store height for each
   
     for (i=0; i<myMessages.length; i++)
     {
          messagesHeights[i] = $('.' + myMessages[i]).outerHeight();
        // console.log(messagesHeights[i]);
          $('.' + myMessages[i]).css('top', -messagesHeights[i]); //move element outside viewport   
     }
}