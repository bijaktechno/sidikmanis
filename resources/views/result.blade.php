@extends('layouts.app')

@section('style')
	<!-- <link rel="stylesheet" href="{{ asset('public/datatable/css/dataTables.bootstrap.min.css')}}" /> -->
	<link rel="stylesheet" href="{{ asset('public/datatable/css/buttons.dataTables.min.css')}}">
	<style type="text/css">
		.tags .label {
			margin-right: 2px;
		}

		.dataTables_paginate{
			width: 100%;;
		}
		#result-table_filter {
		    margin-left: 35px;
		}
		table{
			font-size: 14px;
			/*white-space: nowrap;*/
		}
	</style>
@endsection

@section('content')


<!-- inner page banner -->
<section class="inner-page-banner">
	
</section>
<!-- inner page banner -->

<!-- breadcrumb -->
<ol class="breadcrumb">
	<li class="breadcrumb-item">
		<a href="{{ route('homePage') }}">Home</a>
	</li>
	<li class="breadcrumb-item active">Hasil Diklat</li>
</ol>
<!-- //breadcrumb -->

<!-- Main content -->
<!-- <section class="gallery agile py-5">
	<div class="container-fluid py-lg-5">
		<div class="agile_gallery_grids row w3-agile">
 -->
<section class="gallery agile py-5">
	<div class="container py-lg-5">

		<!-- <div style="width: 100%; padding-left: -10px;"> -->
			<div class="table-responsive">
				<table id="result-table" class="table table-striped table-hover dt-responsive display nowrap" cellspacing="0" style="width: 100%;">
					<thead>
						<tr>
							<th>Nama</th>
							<th>NIP</th>
							<th>Gol</th>
							<th>Jabatan</th>
							<th>Diklat yang Diikuti</th>
							<th>Penyelenggara</th>
							<th>Waktu</th>
							<th>Sumberdana</th>
							<th>Action</th>

						</tr>
					</thead>
				</table>
			</div>
		<!-- </div> -->
	</div>
</section>

<!-- /.main content -->

<!-- 
        <button type="button" class="btn btn-info btn-lg-block w3ls-btn px-4 text-uppercase mr-2" data-toggle="modal"
            aria-pressed="false" data-target="#modalpopup">
            test
        </button> -->

@endsection

@section('modal')

		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-uppercase" id="result-lable">Modal</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="modal-form" action="#" method="post" class="p-3" enctype="multipart/form-data">
					{{ csrf_field() }}

					<div class="form-group">
						<label for="recipient-name1" class="col-form-label">Judul Paper</label>
						<input type="text" class="form-control" placeholder="Judul Paper" name="paper_title" id="paper_title" required="">
					</div>

					<div class="form-group">
						<label for="recipient-name1" class="col-form-label">PDF</label>
						<input type="file" class="form-control-file" placeholder="PDF" name="paper" id="paper">
					</div>

					<div class="right-w3l mt-4 mb-3">
	                    <!-- <button id="modal-button" type="submit" class="form-control">Simpan</button>  -->
	                    <button id="modal-button" type="submit" class="post-button form-control">Simpan</button> 
					</div>
				</form>
			</div>
		</div>
@endsection

@section('script')

	<script type="text/javascript" src="{{ asset('public/datatable/js/jquery.dataTables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/datatable/js/datatables.bootstrap.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/datatable/js/dataTables.buttons.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/datatable/js/buttons.flash.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/datatable/js/pdfmake.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/datatable/js/vfs_fonts.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/datatable/js/buttons.html5.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/datatable/js/buttons.print.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/datatable/js/buttons.colVis.min.js') }}"></script>

	<script type="text/javascript">
		/** Load datatable **/
		var auth ="{{Auth::check()}}";
		$(document).ready(function() {
			get_table_data();

			/** Edit **/
			$("#result-table").on("click", ".paper-button", function(){
				var url = $(this).data("url");

		        $.getJSON(url, function(data, status){

	        		var count = Object.keys(data.userdiklat).length;
		        	
		        	if(status == 'success' && count > 0)
		        	{
						var action = "{{ route('postPaper', 'scd_id') }}";
						// var action = "{{ route('agenda-diklat.update', 'scd_id') }}";
						action = action.replace("scd_id", data.userdiklat.id);

						$("#modal-form").attr("action",action);
						$("#result-lable").text("Edit Data Paper");

	        			$("#paper_title").val(data.userdiklat.paper_title);
		        	}

		        });

			});

			
			$(".post-button").click(function(){

		        var idBtn = "#"+this.id;
				var defaultBtn = $(this).html();
		        var formId = "#"+ $(this).closest('form').attr('id');
		        var formData = $(this).closest('form').serialize();
		        var act = $(this).closest('form').attr('action');
		        var method = $(this).closest('form').attr('method');

				var options = { 
					
				    success:    function(data, status) { 

				        $(idBtn).addClass("disabled");
				        $(idBtn).html("<i class='fa fa-spinner fa-spin'></i> Loading");
				    	// console.log(pesan);
						// var pesan = JSON.parse(pesan);
						
				        if (status == 'success' && data.status == true) {
						  	// $.Notify({style: {background: '#fa6d05', color: '#ffffff'}, content: pesan.message,});
	                    	
		                    $('.alert-success').animate({ top: "0" }, 500).show();
		                    $('.alert-success').html(data.message);

							setTimeout(function(){
								location.reload()
							}, 2000);				
					  	} else {
		        			var arr = data.errors;
		        			var messages = '';

			                $.each(arr, function(index, value)
			                {
			                    if (value.length != 0)
			                    {
			                    	messages += value+"<br>";
			                        // $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
			                    }
			                });

		                    $('.alert-danger').animate({ top: "0" }, 500).show();
		                    $('.alert-danger').html(messages);

		                    setTimeout(function(){
		                        hideAllMessages();
		                        $(idBtn).removeClass("disabled");
		                        $(idBtn).html(defaultBtn);
		                    }, 4000);
					  	}
				    } 
				}; 
				$(formId).ajaxForm(options);	


				// event.preventDefault();

				
			});

		});


		function get_table_data(){
			$('#result-table').DataTable({
				dom: 'Blfrtip',
				buttons: [
				{ extend: 'copy', exportOptions: { columns: ':visible'}},
				{ extend: 'print', exportOptions: { columns: ':visible'}},
				{ extend: 'pdf', orientation: 'landscape', pageSize: 'A4', exportOptions: { columns: ':visible'}},
				{ extend: 'csv', exportOptions: { columns: ':visible'}},
				{ extend: 'colvis', text:'Column'},
				],
				columnDefs: [ {
					targets: -1,
					visible: true
				} ],
				lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
				processing: true,
				serverSide: true,
				ajax: "{{ route('getHasil') }}",
				columns: [
				{data: 'name'},
				{data: 'nip'},
				{data: 'gol'},
				{data: 'position'},
				{data: 'diklat_name'},
				{data: 'organizer'},
				{data: 'diklat_date'},
				{data: 'funding_type'},
				{data: 'action', name: 'action', orderable: false, searchable: false, visible:true},
				// {data: 'schedules.organizer'},
				],
				order: [[4, 'desc']],
			});


		}
	</script>
@endsection








