<!DOCTYPE html>
<html lang="en">
<!-- head -->
<head>
    @include('includes.head')
    @yield('style')
</head>
<!-- /.head -->

<body>

        @include('includes.alert')

    <!-- theme layout -->
    <!-- <div class="theme-layout"> -->
        <!-- header -->
        @include('includes.header')
        <!-- /.header -->

        <!-- first section block -->
        @yield('content')
        <!-- /.first section block -->
        
        <!-- first section modal -->
        @include('includes.modal')
        <!-- /.first section modal -->

        <!-- footer -->
        @include('includes.footer')
        <!-- /. footer -->

    <!-- </div> -->
    <!-- /. theme-layout -->

    <!-- scripts -->
    @include('includes.scripts')
    @yield('script')
    <!-- /. script -->

</body>
</html>