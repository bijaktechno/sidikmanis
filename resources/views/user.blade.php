@extends('layouts.app')

@section('style')
	<!-- <link rel="stylesheet" href="{{ asset('public/datatable/css/dataTables.bootstrap.min.css')}}" /> -->
	<link rel="stylesheet" href="{{ asset('public/datatable/css/buttons.dataTables.min.css')}}">
	<style type="text/css">
		.tags .label {
			margin-right: 2px;
		}

		.dataTables_paginate{
			width: 100%;;
		}
		#user-table_filter {
		    margin-left: 35px;
		}
		table{
			font-size: 14px;
			/*white-space: nowrap;*/
		}
	</style>
@endsection

@section('content')


<!-- inner page banner -->
<section class="inner-page-banner">
	
</section>
<!-- inner page banner -->

<!-- breadcrumb -->
<ol class="breadcrumb">
	<li class="breadcrumb-item">
		<a href="{{ route('homePage') }}">Home</a>
	</li>
	<li class="breadcrumb-item active">Peserta Diklat</li>
</ol>
<!-- //breadcrumb -->

<!-- Main content -->
<!-- <section class="gallery agile py-5">
	<div class="container-fluid py-lg-5">
		<div class="agile_gallery_grids row w3-agile">
 -->
<section class="gallery agile py-5">
	<div class="container py-lg-5">

		<!-- <div style="width: 100%; padding-left: -10px;"> -->
			<div class="table-responsive">
				<table id="user-table" class="table table-striped table-hover dt-responsive display nowrap" cellspacing="0" style="width: 100%;">
					<thead>
						<tr>
							<th>Nama</th>
							<th>NIP</th>
							<th>Gol</th>
							<th>Jabatan</th>
							<th>Created At</th>
							<th>Updated At</th>
							<th style="width: 150px;">Action</th>

						</tr>
					</thead>
				</table>
			</div>
		<!-- </div> -->
	</div>
</section>

<!-- /.main content -->

<!-- 
        <button type="button" class="btn btn-info btn-lg-block w3ls-btn px-4 text-uppercase mr-2" data-toggle="modal"
            aria-pressed="false" data-target="#modalpopup">
            test
        </button> -->

<!-- delete post modal -->
<div id="delete-modal" class="modal modal-danger fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-uppercase" id="exampleModalLabel1">
					<span class="fa-stack fa-sm">
						<i class="fa fa-square-o fa-stack-2x"></i>
						<i class="fa fa-trash fa-stack-1x"></i>
					</span>
					Hapus data ini ?
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
				<form method="post" role="form" id="delete_form">
					{{csrf_field()}}
					{{method_field('DELETE')}}
					<button type="submit" class="btn btn-outline">Delete</button>
				</form>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
</div>
	<!-- /.delete post modal -->

@endsection

@section('modal')

		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-uppercase" id="user-lable">Modal</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="modal-form" action="#" method="post" class="p-3">
	                {{ csrf_field() }}



					<div class="form-row">
		                <div class="form-group col-md-8{{ $errors->has('name') ? ' has-error' : '' }}">
		                    <label for="name">Name</label>
		                    <input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="Name" id="name">
		                    @if ($errors->has('name'))
		                    <span class="help-block">
		                        <strong>{{ $errors->first('name') }}</strong>
		                    </span>
		                    @endif
		                </div>
		                <div class="form-group col-md-4{{ $errors->has('nip') ? ' has-error' : '' }}">
		                    <label for="name">NIP</label>
		                    <input type="number" name="nip" class="form-control" value="{{ old('nip') }}" placeholder="NIP" id="nip">
		                    @if ($errors->has('nip'))
		                    <span class="help-block">
		                        <strong>{{ $errors->first('nip') }}</strong>
		                    </span>
		                    @endif
		                </div>

					</div>

					<div class="form-row">

						<div class="form-group col-md-4">
							<label for="recipient-name1">Golongan</label>
							<select class="form-control" placeholder="Golongan" name="gol" id="gol" required="">
								<option value="I/a">I/a</option>
								<option value="I/b">I/b</option>
								<option value="I/c">I/c</option>
								<option value="I/d">I/d</option>
								<option value="II/a">II/a</option>
								<option value="II/b">II/b</option>
								<option value="II/c">II/c</option>
								<option value="II/d">II/d</option>
								<option value="III/a">III/a</option>
								<option value="III/b">III/b</option>
								<option value="III/c">III/c</option>
								<option value="III/d">III/d</option>
								<option value="IV/a">IV/a</option>
								<option value="IV/b">IV/b</option>
								<option value="IV/c">IV/c</option>
								<option value="IV/d">IV/d</option>
							</select>
						</div>

		                <div class="form-group col-md-8{{ $errors->has('position') ? ' has-error' : '' }}">
		                    <label for="name">Jabatan</label>
		                    <input type="text" name="position" class="form-control" value="{{ old('position') }}" placeholder="Jabatan" id="position">
		                    @if ($errors->has('position'))
		                    <span class="help-block">
		                        <strong>{{ $errors->first('position') }}</strong>
		                    </span>
		                    @endif
		                </div>
					</div>

	                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}" id="username">
	                    <label for="username">Username</label>
	                    <input type="text" name="username" class="form-control" value="{{ old('username') }}" placeholder="Username">
	                    @if ($errors->has('username'))
	                    <span class="help-block">
	                        <strong>{{ $errors->first('username') }}</strong>
	                    </span>
	                    @endif
	                </div>
	                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}" id="email">
	                    <label for="email">Email</label>
	                    <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Email">
	                    @if ($errors->has('email'))
	                    <span class="help-block">
	                        <strong>{{ $errors->first('email') }}</strong>
	                    </span>
	                    @endif
	                </div>
	                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}" id="password">
	                    <label for="password">Password</label>
	                    <input type="password" class="form-control" name="password" placeholder="Password">
	                    @if ($errors->has('password'))
	                    <span class="help-block">
	                        <strong>{{ $errors->first('password') }}</strong>
	                    </span>
	                    @endif
	                </div>
	                <div class="form-group" id="password_confirmation">
	                    <label for="password-confirm">Confirm Password</label>
	                    <input type="password" class="form-control" name="password_confirmation" placeholder="Konfirmasi Password">
	                </div>
					<div class="right-w3l mt-4 mb-3">
	                    <!-- <button id="modal-button" type="submit" class="form-control">Simpan</button>  -->
	                    <button id="modal-button" type="submit" class="post-button form-control">Simpan</button> 
					</div>
				</form>
			</div>
		</div>
@endsection

@section('script')

	<script type="text/javascript" src="{{ asset('public/datatable/js/jquery.dataTables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/datatable/js/datatables.bootstrap.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/datatable/js/dataTables.buttons.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/datatable/js/buttons.flash.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/datatable/js/pdfmake.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/datatable/js/vfs_fonts.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/datatable/js/buttons.html5.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/datatable/js/buttons.print.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/datatable/js/buttons.colVis.min.js') }}"></script>

	<script type="text/javascript">
		/** Load datatable **/
		var auth ="{{Auth::check()}}";

		$(document).ready(function() {
			get_table_data();


			$(".dt-buttons").prepend('<button class="dt-button buttons-add buttons-html5" tabindex="0" aria-controls="user-table" data-toggle="modal" aria-pressed="false" data-target="#modalpopup"><span><i class="fa fa-plus-circle"></i> </span></button>');

			$(".buttons-add").click(function(){

				$("#username").show();
				$("#email").show();
				$("#password").show();
				$("#password_confirmation").show();

				$("#user-lable").text("Tambah Peserta Diklat");
				$("#modal-form").attr("action","{{ route('user-diklat.store') }}");
				// alert('test');

			});


			$(".post-button").click(function(){

		        var idBtn = "#"+this.id;
				var defaultBtn = $(this).html();
		        var formData = $(this).closest('form').serialize();
		        var act = $(this).closest('form').attr('action');
		        var method = $(this).closest('form').attr('method');

		        $(this).addClass("disabled");
		        $(this).html("<i class='fa fa-spinner fa-spin'></i> Loading");


		        $.ajax({
		            url: act,
		            type: method,
		            data: formData,
		            dataType: "json",
			        success:function(data){
		        		// alert(data.message);
		        		if(data.status == true){
	                    	
		                    $('.alert-success').animate({ top: "0" }, 500).show();
		                    $('.alert-success').html(data.message);


			                setTimeout(function () {
			                    $(idBtn).removeClass("disabled");
			                    $(idBtn).html(defaultBtn);
			                    location.reload();
			                }, 2000);
		        			
		        		}
		        		else{
		        			var arr = data.errors;
		        			var messages = '';

			                $.each(arr, function(index, value)
			                {
			                    if (value.length != 0)
			                    {
			                    	messages += value+"<br>";
			                        // $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
			                    }
			                });

		                    $('.alert-danger').animate({ top: "0" }, 500).show();
		                    $('.alert-danger').html(messages);

		                    setTimeout(function(){
		                        hideAllMessages();
		                        $(idBtn).removeClass("disabled");
		                        $(idBtn).html(defaultBtn);
		                    }, 4000);
		        		}
			        },
			        error: function (data) {
	                    $('.alert-danger').animate({ top: "0" }, 500).show();
	                    $('.alert-danger').html('data gagal di simpan');

	                    setTimeout(function(){
	                        hideAllMessages();
	                        $(idBtn).removeClass("disabled");
	                        $(idBtn).html(defaultBtn);
	                    }, 4000);
			        }

		        });

				event.preventDefault();

		        return false;
				
			});


			/** Delete **/
			$("#user-table").on("click", ".delete-button", function(){
				var req_id = $(this).data("id");
				var url = "{{ route('user-diklat.destroy', 'req_id') }}";
				url = url.replace("req_id", req_id);

				$('#delete-modal').modal('show');
				$('#delete_form').attr('action', url);
			});
			
			/** Edit **/
			$("#user-table").on("click", ".edit-button", function(){
				var url = $(this).data("url");

				$("#username").hide();
				$("#email").hide();
				$("#password").hide();
				$("#password_confirmation").hide();

		        $.getJSON(url, function(data, status){
	        		var count = Object.keys(data.userdetail).length;
		        	
		        	if(status == 'success' && count > 0)
		        	{
						var action = "{{ route('user-diklat.update', 'req_id') }}";
						action = action.replace("req_id", data.userdetail.id);

						$("#modal-form").attr("action",action);
						$("#modal-form").attr("method",'put');
						$("#user-lable").text("Edit Peserta Diklat");

	        			$("#name").val(data.userdetail.name);
        				$('#nip').val(data.userdetail.nip);
        				$('#gol').val(data.userdetail.gol);
	        			$("#position").val(data.userdetail.position);
			        	console.log(data);
		        		
		        	}

		        });

			});

		});


		function get_table_data(){
			$('#user-table').DataTable({
				dom: 'Blfrtip',
				buttons: [
				{ extend: 'copy', exportOptions: { columns: ':visible'}},
				{ extend: 'print', exportOptions: { columns: ':visible'}},
				{ extend: 'pdf', orientation: 'landscape', pageSize: 'A4', exportOptions: { columns: ':visible'}},
				{ extend: 'csv', exportOptions: { columns: ':visible'}},
				{ extend: 'colvis', text:'Column'},
				],
				columnDefs: [ {
					targets: -1,
					visible: true
				} ],
				lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
				processing: true,
				serverSide: true,
				ajax: "{{ route('getUser') }}",
				columns: [
				{data: 'name'},
				{data: 'nip'},
				{data: 'gol'},
				{data: 'position'},
				{data: 'created_at'},
				{data: 'updated_at'},
				{data: 'action', name: 'action', orderable: false, searchable: false, visible:true},
				],
				order: [[4, 'desc']],
			});

		}
	</script>
@endsection








