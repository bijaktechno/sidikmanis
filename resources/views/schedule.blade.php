@extends('layouts.app')

@section('style')
	<!-- <link rel="stylesheet" href="{{ asset('public/datatable/css/dataTables.bootstrap.min.css')}}" /> -->
	<link rel="stylesheet" href="{{ asset('public/css/select2.min.css')}}">
	<link rel="stylesheet" href="{{ asset('public/datatable/css/buttons.dataTables.min.css')}}">
	<style type="text/css">
		.tags .label {
			margin-right: 2px;
		}

		.dataTables_paginate{
			width: 100%;;
		}
		#schedule-table_filter {
		    margin-left: 35px;
		}

		table{
			font-size: 14px;
			/*white-space: nowrap;*/
		}

		.select2-container--default .select2-selection--multiple .select2-selection__choice {
			background-color: #3c8dbc ;
			border: 1px solid #367fa9;
			border-radius: 4px;
			cursor: default;
			float: left;
			margin-right: 5px;
			margin-top: 5px;
			padding: 0 5px;
		}
		.select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
			color: black;
			cursor: pointer;
			display: inline-block;
			font-weight: bold;
			margin-right: 4px;
		}

		span.select2.select2-container.select2-container--default{
			width: 100% !important;
			/*width: auto !important;*/
		}
	</style>
@endsection

@section('content')


<!-- inner page banner -->
<section class="inner-page-banner">
	
</section>
<!-- inner page banner -->

<!-- breadcrumb -->
<ol class="breadcrumb">
	<li class="breadcrumb-item">
		<a href="{{ route('homePage') }}">Home</a>
	</li>
	<li class="breadcrumb-item active">Agenda Diklat</li>
</ol>
<!-- //breadcrumb -->

<!-- Main content -->
<!-- <section class="gallery agile py-5">
	<div class="container-fluid py-lg-5">
		<div class="agile_gallery_grids row w3-agile">
 -->
<section class="gallery agile py-5">
	<div class="container py-lg-5">

		<!-- <div style="width: 100%; padding-left: -10px;"> -->
			<div class="table-responsive">
				<table id="schedule-table" class="table table-striped table-hover dt-responsive display nowrap" cellspacing="0" >
					<thead>
						<tr>
							<th>Nama Diklat</th>
							<th>Penyelenggara</th>
							<th>Jumlah Peserta</th>
							<th>Kuota PNS</th>
							<th>Jadwal</th>
							<th>Sifat Pembiayaan</th>
							<th>Kontak Person</th>
							<th>PDF</th>
							<th>Created By</th>
							<th>Created At</th>
							<th>Updated At</th>
							@if(isAdmin())
							<th style="width: 150px;">Action</th>
							@endif
							
						</tr>
					</thead>
				</table>
			</div>
		<!-- </div> -->
	</div>
</section>

<!-- /.main content -->

<!-- 
        <button type="button" class="btn btn-info btn-lg-block w3ls-btn px-4 text-uppercase mr-2" data-toggle="modal"
            aria-pressed="false" data-target="#modalpopup">
            test
        </button> -->

<!-- delete post modal -->
<div id="delete-modal" class="modal modal-danger fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-uppercase" id="exampleModalLabel1">
					<span class="fa-stack fa-sm">
						<i class="fa fa-square-o fa-stack-2x"></i>
						<i class="fa fa-trash fa-stack-1x"></i>
					</span>
					Hapus data ini ?
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
				<form method="post" role="form" id="delete_form">
					{{csrf_field()}}
					{{method_field('DELETE')}}
					<button type="submit" class="btn btn-outline">Delete</button>
				</form>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
</div>
	<!-- /.delete post modal -->

@endsection

@section('modal')

		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-uppercase" id="schedule-lable">Modal</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="modal-form" action="#" method="post" class="p-3" enctype="multipart/form-data">
					{{ csrf_field() }}

					<div class="form-row">

						<div class="form-group col-md-6">
							<label for="recipient-name1" class="col-form-label">Nama Diklat</label>
							<input type="text" class="form-control" placeholder="Nama Diklat" name="diklat_name" id="diklat_name" required="">
						</div>
						
						<div class="form-group col-md-6">
							<label for="recipient-name1" class="col-form-label">Penyelenggara</label>
							<input type="text" class="form-control" placeholder="Penyelenggara" name="organizer" id="organizer" required="">
						</div>
						
					</div>

					<div class="form-row">

						<div class="form-group col-md-6">
							<label for="recipient-name1" class="col-form-label">Jumlah Peserta</label>
							<input type="text" class="form-control" placeholder="Jumlah Peserta" name="participant_qty" id="participant_qty" required="">
						</div>
						<div class="form-group col-md-6">
							<label for="recipient-name1" class="col-form-label">Kuota PNS</label>
							<input type="text" class="form-control" placeholder="Kuota PNS" name="pns_qty" id="pns_qty" required="">
						</div>

					</div>


					<div class="form-group">
						<label for="recipient-name1" class="col-form-label">Peserta Diklat</label>
						<!-- <div class="col-md-12"> -->

							<select class="form-control select2-post-tag" name="user_diklats[]" multiple="multiple" id="user_diklats">
								<option disabled>Select Tags</option>
								@foreach($userdetails as $user)
								<option value="{{ $user->id }}">{{ $user->name }}</option>
								@endforeach
								
							</select>
						<!-- </div> -->
					</div>

					<div class="form-row">
						<div class="form-group col-md-4">
							<label for="recipient-name1" class="col-form-label">Jadwal</label>
							<input type="date" class="form-control" placeholder="Jadwal" name="diklat_date" id="diklat_date" required="">
						</div>
						<div class="form-group col-md-4">
							<label for="recipient-name1" class="col-form-label">Sifat Pembiayaan</label>
							<select class="form-control" placeholder="Sifat Pembiayaan" name="funding_type" id="funding_type" required="">
								<option value="Kontrubusi">Kontrubusi</option>
								<option value="Non Kontrubusi">Non Kontrubusi</option>
							</select>
						</div>
						<div class="form-group col-md-4">
							<label for="recipient-name1" class="col-form-label">Kontak Person</label>
							<input type="text" class="form-control" placeholder="Kontak Person" name="contact_person" id="contact_person" required="">
						</div>
					</div>

					<div class="form-group">
						<label for="recipient-name1" class="col-form-label">PDF</label>
						<input type="file" class="form-control-file" placeholder="PDF" name="pdf_file" id="pdf_file">
					</div>

					<div class="right-w3l mt-4 mb-3">
	                    <!-- <button id="modal-button" type="submit" class="form-control">Simpan</button>  -->
	                    <button id="modal-button" type="submit" class="post-button form-control">Simpan</button> 
					</div>
				</form>
			</div>
		</div>
@endsection

@section('script')
	<script type="text/javascript" src="{{ asset('public/js/select2.min.js') }}"></script>

	<script type="text/javascript" src="{{ asset('public/datatable/js/jquery.dataTables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/datatable/js/datatables.bootstrap.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/datatable/js/dataTables.buttons.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/datatable/js/buttons.flash.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/datatable/js/pdfmake.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/datatable/js/vfs_fonts.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/datatable/js/buttons.html5.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/datatable/js/buttons.print.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/datatable/js/buttons.colVis.min.js') }}"></script>

	<script type="text/javascript">
		/** Load datatable **/
		var auth ="{{isAdmin()}}";

		$(document).ready(function() {


			$('.select2-post-tag').select2();

			get_table_data();


			if(auth){
				$(".dt-buttons").prepend('<button class="dt-button buttons-add buttons-html5" tabindex="0" aria-controls="schedule-table" data-toggle="modal" aria-pressed="false" data-target="#modalpopup"><span><i class="fa fa-plus-circle"></i> </span></button>');
			}

			$(".buttons-add").click(function(){
				$("#schedule-lable").text("Tambah Jadwal Diklat");
				$("#modal-form").attr("action","{{ route('agenda-diklat.store') }}");
				// alert('test');

			});


			$(".post-button").click(function(){

		        var idBtn = "#"+this.id;
				var defaultBtn = $(this).html();
		        var formId = "#"+ $(this).closest('form').attr('id');
		        var formData = $(this).closest('form').serialize();
		        var act = $(this).closest('form').attr('action');
		        var method = $(this).closest('form').attr('method');

				var options = { 
					
				    success:    function(data, status) { 

				        $(idBtn).addClass("disabled");
				        $(idBtn).html("<i class='fa fa-spinner fa-spin'></i> Loading");
				    	// console.log(pesan);
						// var pesan = JSON.parse(pesan);
						
				        if (status == 'success' && data.status == true) {
						  	// $.Notify({style: {background: '#fa6d05', color: '#ffffff'}, content: pesan.message,});
	                    	
		                    $('.alert-success').animate({ top: "0" }, 500).show();
		                    $('.alert-success').html(data.message);

							setTimeout(function(){
								location.reload()
							}, 2000);				
					  	} else {
		        			var arr = data.errors;
		        			var messages = '';

			                $.each(arr, function(index, value)
			                {
			                    if (value.length != 0)
			                    {
			                    	messages += value+"<br>";
			                        // $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
			                    }
			                });

		                    $('.alert-danger').animate({ top: "0" }, 500).show();
		                    $('.alert-danger').html(messages);

		                    setTimeout(function(){
		                        hideAllMessages();
		                        $(idBtn).removeClass("disabled");
		                        $(idBtn).html(defaultBtn);
		                    }, 4000);
					  	}
				    } 
				}; 
				$(formId).ajaxForm(options);	


				// event.preventDefault();

				
			});


			/** Delete **/
			$("#schedule-table").on("click", ".delete-button", function(){
				var scd_id = $(this).data("id");
				var url = "{{ route('agenda-diklat.destroy', 'scd_id') }}";
				url = url.replace("scd_id", scd_id);

				$('#delete-modal').modal('show');
				$('#delete_form').attr('action', url);
			});
			
			/** Edit **/
			$("#schedule-table").on("click", ".edit-button", function(){
				var url = $(this).data("url");

		        $.getJSON(url, function(data, status){
	        		var count = Object.keys(data.schedule).length;
		        	
		        	if(status == 'success' && count > 0)
		        	{
						var action = "{{ route('postAgenda', 'scd_id') }}";
						// var action = "{{ route('agenda-diklat.update', 'scd_id') }}";
						action = action.replace("scd_id", data.schedule.id);

						$("#modal-form").attr("action",action);
						$("#schedule-lable").text("Edit Jadwal Diklat");
						// $("#modal-form").attr("method",'put');

	        			$("#diklat_name").val(data.schedule.diklat_name);
	        			$("#organizer").val(data.schedule.organizer);
	        			$("#participant_qty").val(data.schedule.participant_qty);
	        			$("#pns_qty").val(data.schedule.pns_qty);
	        			$("#diklat_date").val(data.schedule.diklat_date);
	        			$("#funding_type").val(data.schedule.funding_type);
	        			$("#contact_person").val(data.schedule.contact_person);

						$('.select2-post-tag').select2().val(data.userschedule).trigger('change');
						// $('.select2-post-tag').select2().val([2,3]).trigger('change');

			        	// console.log(dataUserschd);
		        		
		        	}

		        });

			});

		});

		function get_table_data(){
			if(auth){
				$('#schedule-table').DataTable({
					dom: 'Blfrtip',
					buttons: [
					{ extend: 'copy', exportOptions: { columns: ':visible'}},
					{ extend: 'print', exportOptions: { columns: ':visible'}},
					{ extend: 'pdf', orientation: 'landscape', pageSize: 'A4', exportOptions: { columns: ':visible'}},
					{ extend: 'csv', exportOptions: { columns: ':visible'}},
					{ extend: 'colvis', text:'Column'},
					],
					columnDefs: [ {
						targets: -1,
						visible: true
					} ],
					lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
					processing: true,
					serverSide: true,
					ajax: "{{ route('getAgenda') }}",
					columns: [
					{data: 'diklat_name'},
					{data: 'organizer'},
					{data: 'participant_qty'},
					{data: 'pns_qty'},
					{data: 'diklat_date'},
					{data: 'funding_type'},
					{data: 'contact_person'},
					{data: 'pdf_file', name: 'pdf_file', orderable: true, searchable: true},
					{data: 'username', name: 'username', orderable: true, searchable: true},
					{data: 'created_at'},
					{data: 'updated_at'},
					{data: 'action', name: 'action', orderable: false, searchable: false, visible:true},
					],
					order: [[4, 'desc']],
				});
			}
			else{
				$('#schedule-table').DataTable({
					dom: 'Blfrtip',
					buttons: [
					{ extend: 'copy', exportOptions: { columns: ':visible'}},
					{ extend: 'print', exportOptions: { columns: ':visible'}},
					{ extend: 'pdf', orientation: 'landscape', pageSize: 'A4', exportOptions: { columns: ':visible'}},
					{ extend: 'csv', exportOptions: { columns: ':visible'}},
					{ extend: 'colvis', text:'Column'},
					],
					columnDefs: [ {
						targets: -1,
						visible: true
					} ],
					lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
					processing: true,
					serverSide: true,
					ajax: "{{ route('getAgenda') }}",
					columns: [
					{data: 'diklat_name'},
					{data: 'organizer'},
					{data: 'participant_qty'},
					{data: 'pns_qty'},
					{data: 'diklat_date'},
					{data: 'funding_type'},
					{data: 'contact_person'},
					{data: 'pdf_file', name: 'pdf_file', orderable: true, searchable: true},
					{data: 'username', name: 'username', orderable: true, searchable: true},
					{data: 'created_at'},
					{data: 'updated_at'},
					{data: 'action', name: 'action', orderable: false, searchable: false, visible:false},
					],
					order: [[4, 'desc']],
				});

			}

		}
	</script>
@endsection








