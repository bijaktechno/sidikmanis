
<!-- js-scripts -->		

	<!-- js -->
	<script type="text/javascript" src="{{ asset('public/js/jquery-2.1.4.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/bootstrap.js') }}"></script> <!-- Necessary-JavaScript-File-For-Bootstrap --> 
	<!-- //js -->	

	<!-- tabs -->
	<script src="{{ asset('public/js/easy-responsive-tabs.js') }}"></script>
	<script>
	$(document).ready(function () {
	$('#horizontalTab').easyResponsiveTabs({
	type: 'default', //Types: default, vertical, accordion           
	width: 'auto', //auto or any width like 600px
	fit: true,   // 100% fit in a container
	closed: 'accordion', // Start closed if in accordion view
	activate: function(event) { // Callback function if tab is switched
	var $tab = $(this);
	var $info = $('#tabInfo');
	var $name = $('span', $info);
	$name.text($tab.text());
	$info.show();
	}
	});
	$('#verticalTab').easyResponsiveTabs({
	type: 'vertical',
	width: 'auto',
	fit: true
	});
	});
	</script>
	<!--//tabs-->
	
	<!-- for-Testimonials -->
	<!-- required-js-files-->
	<link href="{{ asset('public/css/owl.carousel.css') }}" rel="stylesheet">
		<script src="{{ asset('public/js/owl.carousel.js') }}"></script>
			<script>
		$(document).ready(function() {
		  $("#owl-demo").owlCarousel({
			items : 1,
			lazyLoad : true,
			autoPlay : true,
			navigation : false,
			navigationText :  false,
			pagination : true,
		  });
		});
		</script>
	<!--//required-js-files-->
	<!-- //for-Testimonials -->

	<!-- start-smoth-scrolling -->
	<script src="{{ asset('public/js/SmoothScroll.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/move-top.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/easing.js') }}"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
	</script>
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>

	<!-- //here ends scrolling icon -->
	<!-- start-smoth-scrolling -->
	
	<script type="text/javascript" src="{{ asset('public/js/jquery.form.js') }}"></script>
	
	<script type="text/javascript" src="{{ asset('public/js/custom.js') }}"></script>
<!-- //js-scripts -->
