

<!-- Login modal -->
<div class="modal fade bd-example-modal-lg" id="modalpopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">

        @yield('modal')
		
	</div>
</div>
<!-- //Login modal -->

<!-- Login modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-uppercase" id="exampleModalLabel1">Login</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

	            <form method="POST" action="{{ route('login') }}" class="p-3">
	                {{ csrf_field() }}

	                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
	                    <label for="email">Username / Email</label>
	                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus>
	                    @if ($errors->has('email'))
	                    <span class="help-block">
	                        <strong>{{ $errors->first('email') }}</strong>
	                    </span>
	                    @endif
	                </div>

	                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
	                    <label for="password">Password</label>
	                    <input type="password" class="form-control" name="password">
	                    @if ($errors->has('password'))
	                    <span class="help-block">
	                        <strong>{{ $errors->first('password') }}</strong>
	                    </span>
	                    @endif
	                </div>

					<div class="right-w3l mt-4 mb-3">
						<!-- <input type="submit" class="submit-btn form-control" value="Login"> -->
	                    <button id="login-btn" type="submit" class="submit-btn form-control">Login</button> 

					</div>
	                    <!-- <a style="padding-left: 10px" href="{{ route('password.request') }}">Forget password?</a> -->
	            </form>

			</div>
		</div>
	</div>
</div>
<!-- //Login modal -->

<!-- Register modal -->
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-uppercase" id="exampleModalLabel1">Register Here</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{ route('register') }}" method="post" class="p-3">
	                {{ csrf_field() }}



					<div class="form-row">
		                <div class="form-group col-md-8{{ $errors->has('name') ? ' has-error' : '' }}">
		                    <label for="name">Name</label>
		                    <input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="Name">
		                    @if ($errors->has('name'))
		                    <span class="help-block">
		                        <strong>{{ $errors->first('name') }}</strong>
		                    </span>
		                    @endif
		                </div>
		                <div class="form-group col-md-4{{ $errors->has('nip') ? ' has-error' : '' }}">
		                    <label for="name">NIP</label>
		                    <input type="number" name="nip" class="form-control" value="{{ old('nip') }}" placeholder="NIP">
		                    @if ($errors->has('nip'))
		                    <span class="help-block">
		                        <strong>{{ $errors->first('nip') }}</strong>
		                    </span>
		                    @endif
		                </div>

					</div>

					<div class="form-row">

						<div class="form-group col-md-4">
							<label for="recipient-name1">Golongan</label>
							<select class="form-control" placeholder="Golongan" name="gol" required="">
								<option value="I/a">I/a</option>
								<option value="I/b">I/b</option>
								<option value="I/c">I/c</option>
								<option value="I/d">I/d</option>
								<option value="II/a">II/a</option>
								<option value="II/b">II/b</option>
								<option value="II/c">II/c</option>
								<option value="II/d">II/d</option>
								<option value="III/a">III/a</option>
								<option value="III/b">III/b</option>
								<option value="III/c">III/c</option>
								<option value="III/d">III/d</option>
								<option value="IV/a">IV/a</option>
								<option value="IV/b">IV/b</option>
								<option value="IV/c">IV/c</option>
								<option value="IV/d">IV/d</option>
							</select>
						</div>

		                <div class="form-group col-md-8{{ $errors->has('position') ? ' has-error' : '' }}">
		                    <label for="name">Jabatan</label>
		                    <input type="text" name="position" class="form-control" value="{{ old('position') }}" placeholder="Jabatan">
		                    @if ($errors->has('position'))
		                    <span class="help-block">
		                        <strong>{{ $errors->first('position') }}</strong>
		                    </span>
		                    @endif
		                </div>
					</div>

	                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
	                    <label for="username">Username</label>
	                    <input type="text" name="username" class="form-control" value="{{ old('username') }}" placeholder="Username">
	                    @if ($errors->has('username'))
	                    <span class="help-block">
	                        <strong>{{ $errors->first('username') }}</strong>
	                    </span>
	                    @endif
	                </div>
	                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
	                    <label for="email">Email</label>
	                    <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Email">
	                    @if ($errors->has('email'))
	                    <span class="help-block">
	                        <strong>{{ $errors->first('email') }}</strong>
	                    </span>
	                    @endif
	                </div>
	                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
	                    <label for="password">Password</label>
	                    <input type="password" class="form-control" name="password" placeholder="Password">
	                    @if ($errors->has('password'))
	                    <span class="help-block">
	                        <strong>{{ $errors->first('password') }}</strong>
	                    </span>
	                    @endif
	                </div>
	                <div class="form-group">
	                    <label for="password-confirm">Confirm Password</label>
	                    <input type="password" class="form-control" name="password_confirmation" placeholder="Konfirmasi Password">
	                </div>
					<div class="right-w3l mt-4 mb-3">
						<!-- <input type="submit" class="form-control" value="Create account"> -->
	                    <button id="register-btn" type="submit" class="submit-btn form-control">Submit</button> 
					</div>
				</form>

			</div>
		</div>
	</div>
</div>
<!-- //Register modal -->
