
<!-- footer -->
<section class="footer-top pt-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <form action="#" method="post">
                    <input type="email" placeholder="Enter your email..." name="email" required="">
                    <input type="submit" value="Subscribe">
                </form>
            </div>
            <div class="col-lg-6 social-icon mt-lg-0 mt-3 footer">
                <div class="icon-social">
                    <h3>Socialize with us</h3>
                    <a href="#" class="button-footr">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <a href="#" class="button-footr">
                        <i class="fab fa-twitter"></i>
                    </a>
                    <a href="#" class="button-footr">
                        <i class="fab fa-dribbble"></i>
                    </a>
                    <a href="#" class="button-footr">
                        <i class="fab fa-pinterest-p"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="agile-footer middle w3ls-section">
    <div class="container">
        <div class="agileits_w3layouts-footer-bottom">
            <div class="row w3_agile-footer-grids py-5">
                <div class="col-lg-2 col-sm-3 mb-sm-0 mb-4 px-sm-0 px-4 w3_agile-footer1 f1">   
                    <h5 class="mb-3">Useful Links </h5>
                        <ul class="w3ls-footer-bottom-list">
                        <li><a href="about.html">About us</a></li>
                        <li><a href="#">What we do</a></li>
                        <li><a href="projects.html">Projects</a></li>
                        <li><a href="#">Team members</a></li>
                        <li><a href="contact.html">Contact Us</a></li>
                    </ul>
                </div>
                <div class="col-lg-7 col-sm-9 mb-sm-0 mb-4 px-sm-0 px-4 row w3_agile-footer1 f2">
                    <div class="col-lg-3 col-sm-3  mb-sm-0 mb-4 inner-li">
                        <h5 class="mb-3">Account</h5>
                        <ul class="w3ls-footer-bottom-list">
                            <li><a href="#" data-toggle="modal" aria-pressed="false" data-target="#exampleModal">Login</a></li>
                            <li><a href="#" data-toggle="modal" aria-pressed="false" data-target="#exampleModal1">Create account</a></li>
                        </ul>
                    </div>
                    
                    <div class="col-lg-6 col-sm-7  mb-sm-0 mb-4 inner-li">
                        <h5 class="mb-3">Address</h5>
                        <ul class="w3ls-footer-bottom-list">
                            <li> <span class="fas fa-map-marker"></span> {{ $setting->state.' '.$setting->city.' '.$setting->zip }}<span> {{ $setting->country }} </span></li>
                            <li> <span class="fas fa-envelope"></span> <a href="{{ $setting->email }}"> {{ $setting->email }}</a> </li>
                            <li> <span class="fas fa-phone"></span> {{ $setting->phone }} </li>
                            <!-- <li> <span class="fas fa-globe"></span> <a href="#"> www.website.com</a> </li> -->
                            <!-- <li> <span class="fas fa-clock"></span> Working Hours : 8:00 a.m - 6:00 p.m</li> -->
                        </ul>
                    </div>
                    
                    <div class="col-lg-3 col-sm-2 inner-li">
                        <h5 class="mb-3">support</h5>
                        <ul class="w3ls-footer-bottom-list">
                            <li>
                                <a href="#">24/7 Service</a>
                            </li>
                            <li>
                                <a href="#">FAQ's</a>
                            </li>
                            <li>
                                <a href="#">Media Queries</a>
                            </li>
                            <li>
                                <a href="#">Others</a>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-lg-3 col-md-12 px-sm-0 px-4 w3_agile-footer1 f3">
                        <h5 class="mb-3">Latest Tweets</h5>
                    <ul class="tweet-agile">
                        <li>
                            <p class="tweet-p1"><span class="fab fa-twitter" aria-hidden="true"></span><a href="mailto:support@company.com">@example</a> sit amet consectetur adipiscing. <a href="#">http://ax.by/zzzz</a></p>
                            <p class="tweet-p2">Posted 2 days ago.</p>
                        </li>
                        <li>
                            <p class="tweet-p1"><span class="fab fa-twitter" aria-hidden="true"></span><a href="mailto:support@company.com">@example</a> sit amet consectetur adipiscing. <a href="#">http://cx.dy/zzzz</a></p>
                            <p class="tweet-p2">Posted 4 days ago.</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="copyright py-4">
    <div class="agileits_w3layouts-copyright text-center">
        <p>{!! $setting->copyright !!}</p>
    </div>
</section>
<!-- //footer -->