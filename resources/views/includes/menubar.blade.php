
<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mx-auto text-center">
        <li class="nav-item mr-lg-3 {{ request()->is('/') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('homePage') }}" title="{{ $setting->website_title }}">Home
                <span class="sr-only">(current)</span>
            </a>
        </li>
        <li class="nav-item  mr-lg-3 {{ request()->is('kebutuhan-diklat') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('kebutuhan-diklat.index') }}">Kebutuhan Diklat</a>
        </li>
        <!-- <li class="nav-item  mr-lg-3 {{ request()->is('agenda-diklat') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('agenda-diklat.index') }}">Agenda Diklat</a>
        </li>
        <li class="nav-item  mr-lg-3 {{ request()->is('hasil-diklat') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('hasil-diklat.index') }}">Hasil Diklat</a>
        </li>
 -->
        <li class="nav-item dropdown mr-lg-3 {{ request()->is(['agenda-diklat','user-diklat','hasil-diklat']) ? 'active' : '' }}">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                aria-expanded="false">
                Diklat
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                @if(isAdmin())
                <a class="dropdown-item {{ request()->is('user-diklat') ? 'active' : '' }}" href="{{ route('user-diklat.index') }}">Peserta Diklat</a>
                <div class="dropdown-divider"></div>
                @endif
                <a class="dropdown-item {{ request()->is('agenda-diklat') ? 'active' : '' }}" href="{{ route('agenda-diklat.index') }}">Agenda Diklat</a>
                <a class="dropdown-item {{ request()->is('hasil-diklat') ? 'active' : '' }}" href="{{ route('hasil-diklat.index') }}">Hasil Diklat</a>
                <!-- <div class="dropdown-divider"></div> -->
                <!-- <a class="dropdown-item" href="about.html">Stats</a> -->
            </div>
        </li>
        <li class="nav-item mr-lg-3 {{ request()->is('news') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('news.index') }}">Berita</a>
        </li>
        <li class="nav-item {{ request()->is('contact') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('contact') }}">contact</a>
        </li>
    </ul>
    <div class="buttons">


        @if(Auth::check())
        <!-- <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span style="color: #007bbd"> Logout</span></a></li> -->
        <a href="{{ route('logout') }}" type="button" class="btn btn-info btn-lg-block w3ls-btn px-4 text-uppercase mr-2" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            Logout
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
        @else

        <button type="button" class="btn btn-info btn-lg-block w3ls-btn px-4 text-uppercase mr-2" data-toggle="modal"
            aria-pressed="false" data-target="#exampleModal">
            Login
        </button>
        <button type="button" class="btn btn-info btn-lg-block w3ls-btn1 px-4 text-uppercase" data-toggle="modal"
            aria-pressed="false" data-target="#exampleModal1">
            Register
        </button>
        @endif
    </div>
    
</div>