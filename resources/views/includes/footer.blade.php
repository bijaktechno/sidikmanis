
<!-- footer -->
<section class="footer-top pt-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
             <!--    <form action="#" method="post">
                    <input type="email" placeholder="Enter your email..." name="email" required="">
                    <input type="submit" value="Subscribe">
                </form> -->
            </div>
            <div class="col-lg-6 social-icon mt-lg-0 mt-3 footer">
                <div class="icon-social">
                    <h3>Media Sosial Sidikmanis</h3>
                    <a href="{{ $setting->facebook }}" target="_blank" class="button-footr">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <a href="{{ $setting->twitter }}" target="_blank" class="button-footr">
                        <i class="fab fa-twitter"></i>
                    </a>
                  
                </div>
            </div>
        </div>
    </div>
</section>
<section class="agile-footer middle w3ls-section">
    <div class="container">
        <div class="agileits_w3layouts-footer-bottom">
            <div class="row w3_agile-footer-grids py-5">
                <div class="col-lg-2 col-sm-3 mb-sm-0 mb-4 px-sm-0 px-4 w3_agile-footer1 f1">   
                    <h5 class="mb-3">Ringkas Menu </h5>
                        <ul class="w3ls-footer-bottom-list">
                        <li><a href="{{ route('homePage') }}">Beranda</a></li>
                        <li><a href="{{ route('kebutuhan-diklat.index') }}">Kebutuhan Diklat</a></li>
                        <li><a href="{{ route('agenda-diklat.index') }}">Agenda Diklat</a></li>
                        <li><a href="{{ route('hasil-diklat.index') }}">Hasil Diklat</a></li>
                        <li><a href="{{ route('news.index') }}">Berita</a></li>
                        <li><a href="{{ route('contact') }}">Kontak</a></li>
                    </ul>
                </div>
                <div class="col-lg-7 col-sm-9 mb-sm-0 mb-4 px-sm-0 px-4 row w3_agile-footer1 f2">
                    <div class="col-lg-3 col-sm-3  mb-sm-0 mb-4 inner-li">
                        <h5 class="mb-3">Account</h5>
                        <ul class="w3ls-footer-bottom-list">
                            <li><a href="#" data-toggle="modal" aria-pressed="false" data-target="#exampleModal">Login</a></li>
                            <li><a href="#" data-toggle="modal" aria-pressed="false" data-target="#exampleModal1">Create account</a></li>
                        </ul>
                    </div>
                    
                    <div class="col-lg-6 col-sm-7  mb-sm-0 mb-4 inner-li">
                        <h5 class="mb-3">Address</h5>
                        <ul class="w3ls-footer-bottom-list">
                            <li> <span class="fas fa-map-marker"></span> {{ $setting->address_line_one }}</li>
                            <li> <span class="fas fa-envelope"></span> <a href="{{ $setting->email }}"> {{ $setting->email }}</a> </li>
                            <li> <span class="fas fa-phone"></span> {{ $setting->phone }} </li>
                            <!-- <li> <span class="fas fa-globe"></span> <a href="#"> www.website.com</a> </li> -->
                            <!-- <li> <span class="fas fa-clock"></span> Working Hours : 8:00 a.m - 6:00 p.m</li> -->
                        </ul>
                    </div>
                    
                   <!--  <div class="col-lg-3 col-sm-2 inner-li">
                        <h5 class="mb-3">support</h5>
                        <ul class="w3ls-footer-bottom-list">
                            <li>
                                <a href="#">24/7 Service</a>
                            </li>
                            <li>
                                <a href="#">FAQ's</a>
                            </li>
                            <li>
                                <a href="#">Media Queries</a>
                            </li>
                            <li>
                                <a href="#">Others</a>
                            </li>
                        </ul>
                    </div> -->
                    <div class="clearfix"></div>
                </div>
                <div class="col-lg-3 col-md-12 px-sm-0 px-4 w3_agile-footer1 f3">
                        <h5 class="mb-3">Tweets Terakhir</h5>
                    <a class="twitter-timeline" href="{{ $setting->twitter }}" data-tweet-limit="2" data-theme="dark" data-link-color="#57C8EB" data-chrome="noheader nofooter noscrollbar noborders transparent">Tweets by BKPSDM Kab. Purwakarta</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

                </div>
            </div>
        </div>
    </div>
</section>
<section class="copyright py-4">
    <div class="agileits_w3layouts-copyright text-center">
        <p>{!! $setting->copyright !!}</p>
    </div>
</section>
<!-- //footer -->