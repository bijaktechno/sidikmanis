
<!-- header -->
<header>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="{{ route('homePage') }}" title="{{ $setting->website_title }}">
                <!-- <i class="fas fa-boxes"></i> -->{{ $setting->website_title }}
            </a>
            <button class="navbar-toggler ml-md-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>




            <!-- Menu Bar -->
            @include('includes.menubar')
            <!-- Menu Bar -->

        </nav>
    </div>
</header>
<!-- //header -->
