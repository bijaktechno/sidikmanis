@extends('layouts.app')

@section('title', $setting->meta_title)
@section('keywords', $setting->meta_keywords)
@section('description', $setting->meta_description)

@section('style')
@endsection

@section('content')
<!-- banner slider-->
<div class="banner-silder">
	<div id="JiSlider" class="jislider">
		<ul>
			<li>
				<div class="w3layouts-banner-top">
					<div class="bs-slider-overlay">
						<div class="container">
							<!-- Slide Text Layer -->
							<div class="w3l-slide-text text-center">
								<h1 class="text-uppercase pt-4 pb-3">SIDIKMANIS</h1>
								<p class="heading_bottom mb-4">Sistem Informasi Diklat Manajemen dan Teknis</p>
								
							</div>
						</div>
					</div>
				</div>
			</li>
			<li>
				<div class="w3layouts-banner-top w3layouts-banner-top1">
					<div class="bs-slider-overlay">
						<div class="container">
							<!-- Slide Text Layer -->
							<div class="w3l-slide-text text-center">
								<h2 class="text-uppercase pt-4 pb-3">Ruang Kelas Diklat Manajemen</h2>
								<p class="heading_bottom mb-4">PUSDIKMIN POLRI</p>
								
							</div>
						</div>
					</div>
				</div>
			</li>
			<li>
				<div class="w3layouts-banner-top w3layouts-banner-top2">
					<div class="bs-slider-overlay">
						<div class="container">
							<!-- Slide Text Layer -->
							<div class="w3l-slide-text text-center">
								<h3 class="text-uppercase pt-4 pb-3">Pembukaan Diklat</h3>
								<p class="heading_bottom mb-4">PUSDIKMIN POLRI</p>
								
							</div>
						</div>
					</div>
				</div>
			</li>
			<li>
				<div class="w3layouts-banner-top w3layouts-banner-top3">
					<div class="bs-slider-overlay">
						<div class="container">
							<!-- Slide Text Layer -->
							<div class="w3l-slide-text text-center">
								<h3 class="text-uppercase pt-4 pb-3">Perpisahan DIklat Manajemen</h3>
								<p class="heading_bottom mb-4">PPSDM KEMENDAGRI</p>
								
							</div>
						</div>
					</div>
				</div>
			</li>
			<li>
				<div class="w3layouts-banner-top w3layouts-banner-top4">
					<div class="bs-slider-overlay">
						<div class="container">
							<!-- Slide Text Layer -->
							<div class="w3l-slide-text text-center">
								<h3 class="text-uppercase pt-4 pb-3">Ruang Kelas Diklat</h3>
								<p class="heading_bottom mb-4">PUSDIKMIN POLRI</p>
								
							</div>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
</div>
<!-- //banner slider -->


<!-- Blog -->
<section class="blog py-5">
	<div class="container-fluid py-lg-5">
		<div class="row about_grids">
			<div class="col-lg-4 col-md-12 mb-lg-0 mb-4 team_grid1">
				<h3 class="heading text-uppercase">Berita</h3>
				<p>Sistem Informasi Diklat Manegerial dan Teknis</p>
				<p class="second_para"> Informasi terkini sekitar Diklat.</p>

				<!-- <button type="button" class="btn btn-secondary btn-lg btn-block">Tambah Berita</button> -->
			</div>
			<div class="col-lg-8 blog-posts">
				<div class="row">

				@foreach($news as $post)
					<div class="col-md-4 mb-md-0 mb-5">
						<p class="date mb-2">{{ date("d F Y", strtotime($post->post_date)) }}</p>
						<img src="{{ get_featured_image_thumbnail_url($post->featured_image) }}" alt="" class="img-fluid">
						<h4 class="mt-4">{{ str_limit($post->post_title, 20) }}</h4>
						<p class="my-2"> {!! str_limit($post->post_details, 60) !!}</p>
				@if(isAdmin())
						<a href="#" data-url="{{ route('news.edit', $post->id) }}" class="edit-button" data-toggle="modal"
		            aria-pressed="false" data-target="#modalpopup"><span class="fa fa-edit"></span></a>

		        @endif
						<a href="#" data-url="{{ route('news.edit', $post->id) }}" class="view-button" data-toggle="modal"
		            aria-pressed="false" data-target="#detail-modal">Read More <span class="fa fa-angle-right"></span></a>
					</div>

				@endforeach

				</div>
				

			</div>
		</div>
	</div>
</section>
<!-- //Blog -->
		
<!-- services -->
<section class="services">
	<div class="service-overlay py-5">
	<div class="container-fluid py-lg-5 py-3">
		<div class="row service_grids">
			<div class="col-lg-8">
				<div class="row">
					
					<div class="col-lg-4 col-md-6 mb-lg-0 mb-5 ser_grid1 icon1">
						<span class="fa fa-users"></span>
						<h3>Peserta</h3>
						<ul>
						<li>Pejbat pimpinan tinggi pratama</li>
						<li>Pejbat administrasi</li>
						<li>Pejbat fungsional</li>
						</ul>
						<!-- <a href="#">Read More <span class="fa fa-angle-right"></span></a> -->
					</div>
					<div class="col-lg-4 col-md-6 mb-lg-0 mb-5 ser_grid1 icon2">
						<span class="fa fa-bullhorn"></span>
						<h3>Fasilitator</h3>
						<p>Pejabat strukturan yang berpengalaman dalam bidangnya</p>
						<!-- <a href="#">Read More <span class="fa fa-angle-right"></span></a> -->
					</div>
					<div class="col-lg-4 col-md-6 mb-lg-0 mb-5 ser_grid1 icon3">
						<span class="fa fa-clock"></span>
						<h3>Waktu Pelaksanaan</h3>
						<p>3 Hari Efektif</p>
						<!-- <a href="#">Read More <span class="fa fa-angle-right"></span></a> -->
					</div>
					<div class="col-lg-4 col-md-6 mb-lg-0 mb-5 ser_grid1 icon4">
						<span class="fa fa-cogs"></span>
						<h3>Metode Pelatihan</h3>
						<p>Ceramah, presentasi, dan diskusi pembutan rencana implementasi latihan dan simulasi</p>
						<!-- <a href="#">Read More <span class="fa fa-angle-right"></span></a> -->
					</div>
					<div class="col-lg-4 col-md-6 mb-lg-0 mb-5 ser_grid1 icon1">
						<span class="fa fa-edit"></span>
						<h3>Materi Pokok</h3>
						
						<ul>
						<li>Studi kasus</li>
						<li>Identifiksi indikator kinerja utama</li>
						<li>Penilaian kinerja Pegawai Negeri Sipil</li>
						<li>Perencanaan Kinerja Pegawai Negeri Sipil</li>
						<li>Pengantar Manajemen Kinerja Aparatur Sipil Negara</li>
						<li>Tatacara Penyusunan SKP Jabatan Fungsional</li>
						</ul>
						<!-- <a href="#">Read More <span class="fa fa-angle-right"></span></a> -->
					</div>
					<div class="col-lg-4 col-md-6 mb-lg-0 mb-5 ser_grid1 icon2">
						<span class="fa fa-tag"></span>
						<h3>Biaya</h3>
						
						<ul>
						<li>Di Pusat Pengembangan ASN BKN* Rp 2.327.000</li>
						<li>Di luar Pusat Pengembangan ASN BKN** Rp 1.830.000</li>
						</ul>
						<!-- <a href="#">Read More <span class="fa fa-angle-right"></span></a> -->
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="row">

					<div class="col-lg-12 col-12 mt-lg-0 mt-5 team_grid1">
						<h3 class="heading text-uppercase">Program Teknis Manajemen Kinerja</h3>
						<p>Pelatihan ini memberikan pemahaman kepad pejabat pimpinan tinggi pratama mengenai konsep manajemen kinerja yang mutakhir, mampu memperbaiki kualitas indikator kinerja utama, memahami berbagai metode peningkatan prestasi kerja pegwai, mampu memahami berbagai studi kasus dan praktek terbaik manajemen kinerja, perenncanaan kinerja dan penilaian kinerja di instansinya</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</section>
<!-- //services -->

<!-- Team -->
<section class="team py-5 my-lg-5 my-3">
	<div align="center">
			<h2>Regulasi</h2><br><hr><br>
		</div>
	<div class="container-fluid ">
	<div class="row service_grids">
		
			<div class="col-lg-6">

				

				
						<h5 class="heading text-uppercase">Perkalan No. 4 Tahun 2013</h5>
						<p>
							<object data="{{ get_pdf_url('Perkalan-No.4-Tahun-2013-Tentang-Pedoman-Penyelenggaraan-Diklat-Teknis-Penyelenggaraan-Diklat.pdf') }}" type="application/pdf" width="100%" height="500px">
						      <p>Alternative text - include a link <a href="{{ get_pdf_url('Perkalan-No.4-Tahun-2013-Tentang-Pedoman-Penyelenggaraan-Diklat-Teknis-Penyelenggaraan-Diklat.pdf') }}">to the PDF!</a></p>
						    </object>

						</p>
						
					
				

			</div>


			<div class="col-lg-6">
				
					
					<h5 class="heading text-uppercase">Perkalan No. 5 Tahun 2013</h5>
						<p>
							<object data="{{ get_pdf_url('Perkalan-No.5-Tahun-2013-Tentang-Pedoman-Penyelenggaraan-Diklat-Teknis-Pengelola-Diklat.pdf') }}" type="application/pdf" width="100%" height="500px">
						      <p>Alternative text - include a link <a href="{{ get_pdf_url('Perkalan-No.5-Tahun-2013-Tentang-Pedoman-Penyelenggaraan-Diklat-Teknis-Pengelola-Diklat.pdf') }}">to the PDF!</a></p>
						    </object>
							<!-- <embed src="../../public/file/Perkalan-No.5-Tahun-2013-Tentang-Pedoman-Penyelenggaraan-Diklat-Teknis-Pengelola-Diklat.pdf" width="100%" height="500px"></embed> -->
						</p>



					<!-- <h1>PDF.js 'Hello, world!' example</h1> -->

					<!-- <canvas id="the-canvas" width="100%" height="500px"></canvas> -->
			
			</div>
			<!-- <div class="col-lg-2"> -->
				<!-- <div class="row"> -->

				<!-- </div> -->
			<!-- </div> -->
		</div>
	</div>
</section>
<!-- //Team -->


<div id="detail-modal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-uppercase" id="exampleModalLabel1">
					title
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>

			</div>
			<div class="modal-body">
				<!-- <img id="modal-imagae" src="" alt="" class="img-fluid"> -->
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
</div>

@endsection

@section('modal')

		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-uppercase" id="exampleModalLabel1">Berita</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form  id="modal-form" action="{{ route('news.store') }}" method="post" enctype="multipart/form-data" class="p-3">
				{{ csrf_field() }}
					
					<div class="form-group{{ $errors->has('post_title') ? ' has-error' : '' }}">
						<label for="post_title" class="control-label">News Title</label>
							<input type="text" name="post_title" class="form-control" id="post_title" value="{{ old('post_title') }}" placeholder="ex: News title">
							@if ($errors->has('post_title'))
							<span class="help-block">
								<strong>{{ $errors->first('post_title') }}</strong>
							</span>
							@endif
					</div>
					<div id="form-news-slug" class="form-group{{ $errors->has('post_slug') ? ' has-error' : '' }}">
						<label for="post_slug" class="control-label">News Slug</label>
							<input type="text" name="post_slug" class="form-control" id="post_slug" value="{{ old('post_slug') }}" placeholder="ex: news-slug">
							@if ($errors->has('post_slug'))
							<span class="help-block">
								<strong>{{ $errors->first('post_slug') }}</strong>
							</span>
							@endif
					</div>

					<div class="form-group{{ $errors->has('post_date') ? ' has-error' : '' }}">
						<label for="post_date" class="control-label">News Date</label>
							<input type="date" name="post_date" class="form-control pull-right" id="post_date">
							
							@if ($errors->has('post_date'))
							<span class="help-block">
								<strong>{{ $errors->first('post_date') }}</strong>
							</span>
							@endif
					</div>
					<div class="form-group{{ $errors->has('publication_status') ? ' has-error' : '' }}">
						<label for="publication_status" class="control-label">Publication Status</label>
							<select name="publication_status" class="form-control" id="publication_status">
								<option value="" selected disabled>Select One</option>
								<option value="1">Published</option>
								<option value="0">Unpublished</option>
							</select>
							@if ($errors->has('publication_status'))
							<span class="help-block">
								<strong>{{ $errors->first('publication_status') }}</strong>
							</span>
							@endif
					</div>
					<div class="form-group{{ $errors->has('is_featured') ? ' has-error' : '' }}">
						<label for="is_featured" class="control-label">Is Featured ?</label>
							<label class="radio-inline">
								<input type="radio" name="is_featured" id="is_featured_1" value="1" {{ old('is_featured') == 1 ? 'checked' : '' }}>Yes
							</label>
							<label class="radio-inline">
								<input type="radio" name="is_featured" id="is_featured_2" value="0" {{ old('is_featured') == 0 ? 'checked' : '' }}>No
							</label>
							@if ($errors->has('is_featured'))
							<span class="help-block">
								<strong>{{ $errors->first('is_featured') }}</strong>
							</span>
							@endif
					</div>
					<div class="form-group{{ $errors->has('featured_image') ? ' has-error' : '' }}">
						<label for="featured_image" class="control-label">Featured Image</label>
							<input type="file" name="featured_image" id="featured_image" class="form-control-file">
							<p class="help-block">Example block-level help text here.</p>
							@if ($errors->has('featured_image'))
							<span class="help-block">
								<strong>{{ $errors->first('featured_image') }}</strong>
							</span>
							@endif
					</div>

					<div class="form-group{{ $errors->has('post_details') ? ' has-error' : '' }}">
						<label for="post_details" class="control-label">News Details</label>
							<textarea name="post_details" class="form-control summernote" id="post_details">{{ old('post_details') }}</textarea>
							@if ($errors->has('post_details'))
							<span class="help-block">
								<strong>{{ $errors->first('post_details') }}</strong>
							</span>
							@endif
					</div>

					<div class="form-group{{ $errors->has('post_title') ? ' has-error' : '' }}">
						<label class="control-label"></label>
							<div class="bs-callout bs-callout-success">
								<h4>SEO Information</h4>
							</div>
					</div>
					<div class="form-group{{ $errors->has('meta_title') ? ' has-error' : '' }}">
						<label for="meta_title" class="control-label">Meta Title</label>
							<input type="text" name="meta_title" class="form-control" id="meta_title" value="{{ old('meta_title') }}" placeholder="ex: News title">
							@if ($errors->has('meta_title'))
							<span class="help-block">
								<strong>{{ $errors->first('meta_title') }}</strong>
							</span>
							@endif
					</div>
					<div class="form-group{{ $errors->has('meta_keywords') ? ' has-error' : '' }}">
						<label for="meta_keywords" class="control-label">Meta Keywords</label>
							<input type="text" name="meta_keywords" class="form-control" id="meta_keywords" value="{{ old('meta_keywords') }}" placeholder="ex: news, title">
							@if ($errors->has('meta_keywords'))
							<span class="help-block">
								<strong>{{ $errors->first('meta_keywords') }}</strong>
							</span>
							@endif
					</div>
					<div class="form-group{{ $errors->has('meta_description') ? ' has-error' : '' }}">
						<label for="meta_description" class="control-label">Meta Description</label>
							<textarea name="meta_description" id="meta_description" class="form-control" rows="3" placeholder="ex: News dscription">{{ old('meta_description') }}</textarea>
							@if ($errors->has('meta_description'))
							<span class="help-block">
								<strong>{{ $errors->first('meta_description') }}</strong>
							</span>
							@endif
					</div>
					<div class="right-w3l mt-4 mb-3">
	                    <button id="modal-button" type="submit" class="post-button form-control">Simpan</button> 
					</div>
				</form>

			</div>
		</div>
@endsection

@section('script')

<!-- <script src="//mozilla.github.io/pdf.js/build/pdf.js"></script> -->


<script type="text/javascript">
/*var url = '../../public/file/Perkalan-No.5-Tahun-2013-Tentang-Pedoman-Penyelenggaraan-Diklat-Teknis-Pengelola-Diklat.pdf';

// Loaded via <script> tag, create shortcut to access PDF.js exports.
var pdfjsLib = window['pdfjs-dist/build/pdf'];

// The workerSrc property shall be specified.
pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

// Asynchronous download of PDF
var loadingTask = pdfjsLib.getDocument(url);
loadingTask.promise.then(function(pdf) {
  console.log('PDF loaded');
  
  // Fetch the first page
  var pageNumber = 1;
  pdf.getPage(pageNumber).then(function(page) {
    console.log('Page loaded');
    
    var scale = 1.5;
    var viewport = page.getViewport(scale);

    // Prepare canvas using PDF page dimensions
    var canvas = document.getElementById('the-canvas');
    var context = canvas.getContext('2d');
    canvas.height = viewport.height;
    canvas.width = viewport.width;

    // Render PDF page into canvas context
    var renderContext = {
      canvasContext: context,
      viewport: viewport
    };
    var renderTask = page.render(renderContext);
    renderTask.then(function () {
      console.log('Page rendered');
    });
  });
}, function (reason) {
  // PDF loading error
  console.error(reason);
});*/
</script>

<!-- Banner Slider js script file-->
<script src="{{ asset('public/js/JiSlider.js') }}"></script>
<script>
$(window).load(function () {
	$('#JiSlider').JiSlider({
		color: '#fff',
		start: 1,
		reverse: false
	}).addClass('ff')
})
</script>
<script>
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-36251023-1']);
_gaq.push(['_setDomainName', 'jqueryscript.net']);
_gaq.push(['_trackPageview']);

(function () {
	var ga = document.createElement('script');
	ga.type = 'text/javascript';
	ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(ga, s);
})();
</script>

<script type="text/javascript">
$(document).ready(function(){

	$(".post-button").click(function(){

        var idBtn = "#"+this.id;
		var defaultBtn = $(this).html();
        var formId = "#"+ $(this).closest('form').attr('id');
        var formData = $(this).closest('form').serialize();
        var act = $(this).closest('form').attr('action');
        var method = $(this).closest('form').attr('method');

		var options = { 
			
		    success:    function(data, status) { 

		        $(idBtn).addClass("disabled");
		        $(idBtn).html("<i class='fa fa-spinner fa-spin'></i> Loading");
		    	// console.log(pesan);
				// var pesan = JSON.parse(pesan);
				
		        if (status == 'success' && data.status == true) {
				  	// $.Notify({style: {background: '#fa6d05', color: '#ffffff'}, content: pesan.message,});
                    $('.alert-success').animate({ top: "0" }, 500).show();
                    $('.alert-success').html(data.message);

					setTimeout(function(){
						location.reload()
					}, 2000);				
			  	} else {
        			var arr = data.errors;
        			var messages = '';

	                $.each(arr, function(index, value)
	                {
	                    if (value.length != 0)
	                    {
	                    	messages += value+"<br>";
	                        // $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
	                    }
	                });

                	
                    $('.alert-danger').animate({ top: "0" }, 500).show();
                    $('.alert-danger').html(messages);

                    setTimeout(function(){
                        hideAllMessages();
                        $(idBtn).removeClass("disabled");
                        $(idBtn).html(defaultBtn);
                    }, 4000);
                    
			  	}
		    }, error: function (data, status) {
        		
    			var arr = data.responseJSON.errors;
    			var messages = '';

    			console.log(data);


                $.each(arr, function(index, value)
                {
                    if (value.length != 0)
                    {
                    	messages += value+"<br>";
                    }
                });

            	
                $('.alert-danger').animate({ top: "0" }, 500).show();
                $('.alert-danger').html(messages);

                setTimeout(function(){
                    hideAllMessages();
                    $(idBtn).removeClass("disabled");
                    $(idBtn).html(defaultBtn);
                }, 4000);
	        } 
		}; 
		$(formId).ajaxForm(options);	


		// event.preventDefault();

		
	});

	/*$("#add-button").on('click', function(){
		
		$("#form-news-slug").removeAttr('style');

	});*/

	$(".view-button").on("click",  function(){
		var url = $(this).data("url");
        $.getJSON(url, function(data, status){
        	if(status == 'success')
        	{
				var image = "{{ get_featured_image_url() }}";
				image = '<img id="modal-imagae" src="'+image+"/"+data.news.featured_image+'" alt="" class="img-fluid">';

				var detail = data.news.post_details;
				// console.log(image);
				// action = action.replace("'featured_image'", data.news.id);

        		$("#detail-modal .modal-title").html(data.news.post_title);
        		$("#detail-modal .modal-body").html(image+detail);

        	}
        });

	});

	/** Edit **/
	$(".edit-button").on("click",  function(){
		var url = $(this).data("url");

        $.getJSON(url, function(data, status){
        	if(status == 'success')
        	{
				var action = "{{ route('postNews', 'scd_id') }}";
				// var action = "{{ route('agenda-diklat.update', 'scd_id') }}";
				action = action.replace("scd_id", data.news.id);

				// $("#form-news-slug").css('display', 'none')
				// alert(data.news.publication_status);

				$("#modal-form").attr("action",action);

    			$("#post_date").val(data.news.post_date);
    			$("#post_title").val(data.news.post_title);
    			$("#post_slug").val(data.news.post_slug);
    			$("#post_details").val(data.news.post_details);
    			$("#publication_status").val(data.news.publication_status);

    			// $("#is_featured").val(data.news.is_featured);
    			$("input[name=is_featured][value=" + data.news.is_featured + "]").prop('checked', true);

    			$("#meta_title").val(data.news.meta_title);
    			$("#meta_keywords").val(data.news.meta_keywords);
    			$("#meta_description").val(data.news.meta_description);
	        	// console.log(data);
        		
        	}

        });

	});
});	
</script>


	<!-- //Banner Slider js script file-->

@endsection








