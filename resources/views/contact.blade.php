@extends('layouts.app')

@section('style')
@endsection

@section('content')


<!-- inner page banner -->
<section class="inner-page-banner">
	
</section>
<!-- inner page banner -->

<!-- breadcrumb -->
<ol class="breadcrumb">
	<li class="breadcrumb-item">
		<a href="index.html">Home</a>
	</li>
	<li class="breadcrumb-item active">Contact</li>
</ol>
<!-- //breadcrumb -->
	
<!-- contact -->
<section class="contact py-5">
	<div class="container-fluid pt-lg-5">
		<div class="row contact-grids">
			<div class="col-lg-4 col-12 mb-lg-0 mb-sm-4 mb-3 team_grid1">
				<h1 class="heading text-uppercase">Kontak Kami</h1>

				<p>Alamat</p>
				{!! $setting->address_line_one !!}
				<!-- <p class="second_para">Jl. Veteran, Komplek Perum Hegarmanah Kel. Ciseureuh, Kec. Purwakarta, Kab. Purwakarta, Jawa Barat 41118</p> -->
				
				<p> Telpon Kantor : {{ $setting->phone }}
				<br>Fax : {{ $setting->fax }}<br>
				Whatsapp : {{ $setting->mobile }} </p>

			</div>
			
			<div class="col-lg-8 col-md-6 mt-md-0 mt-4 map">
				{!! $setting->map_iframe !!}
			</div>
		</div>
	</div>
</section>
<!-- //contact -->


@endsection

@section('modal')

		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-uppercase" id="exampleModalLabel1">Login tets</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="#" method="post" class="p-3">
					<div class="form-group">
						<label for="recipient-name" class="col-form-label">User Name</label>
						<input type="email" class="form-control" placeholder="User Name" name="Name" id="recipient-name" required="">
					</div>
					<div class="form-group">
						<label for="recipient-name1" class="col-form-label">Password</label>
						<input type="password" class="form-control" placeholder="Password" name="Name" id="recipient-name1" required="">
					</div>
					<div class="right-w3l mt-4 mb-3">
						<input type="submit" class="form-control" value="Login">
					</div>
				</form>

			</div>
		</div>
@endsection

@section('script')


@endsection








