<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userdetail extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'nip', 'gol', 'position', 'phone', 'address'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }
    
	public function schedules() {
		return $this->belongsToMany(Schedule::class);
	}

    public function userschedule() {
        return $this->hasMany(ScheduleUserdetail::class);
    }
}
