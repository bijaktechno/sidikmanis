<?php

namespace App\Providers;

use App\Setting;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider {
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot() {
		//View::share('setting', 'query');
        Schema::defaultStringLength(191);

        View::composer(['includes.head', 'includes.header', 'includes.footer'], function ($view) {
            $setting = Setting::first();
            $view->with(compact('setting'));
        });
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register() {
		//
	}
}
