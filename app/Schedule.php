<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
	protected $fillable = [
		'user_id', 'diklat_name', 'organizer', 'participant_qty', 'pns_qty', 'diklat_date', 'funding_type', 'contact_person', 'file'
	];

	public function user() {
		return $this->belongsTo(User::class);
	}

	public function userdetails() {
		return $this->belongsToMany(Userdetail::class);
	}

    public function userschedule() {
        return $this->hasMany(ScheduleUserdetail::class);
    }
}
