<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Image;
use Purifier;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(isAdmin()){

            $news = News::orderBy('post_date', 'desc')->paginate(12);
        }
        else{
            $news = News::where('publication_status', 1)->orderBy('post_date', 'desc')->paginate(12);
        }

        return view('news', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = request()->validate([
            'post_date' => 'required|date',
            'post_title' => 'required|string|max:255',
            'post_slug' => 'required|alpha_dash|min:5|max:150|unique:news',
            'post_details' => 'required|string',
            'featured_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10240|dimensions:max_width=10000,max_height=10000',
            'publication_status' => 'required',
            'is_featured' => 'required',
            'meta_title' => 'required|max:250',
            'meta_keywords' => 'required|max:250',
            'meta_description' => 'required|max:400',
        ], [
            'featured_image.dimensions' => 'Max dimensions 350x600',
        ]);

        $post = News::create([
            'user_id' => Auth::user()->id,
            'post_title' => $request->input('post_title'),
            'post_slug' => $request->input('post_slug'),
            'post_date' => $request->input('post_date'),
            'publication_status' => $request->input('publication_status'),
            'is_featured' => $request->input('is_featured'),
            'post_details' => Purifier::clean($request->input('post_details')),
            'meta_title' => $request->input('meta_title'),
            'meta_keywords' => $request->input('meta_keywords'),
            'meta_description' => Purifier::clean($request->input('meta_description')),
        ]);

        if ($request->hasFile('featured_image')) {
            $image = $request->file('featured_image');
            $file_name = $this->featured_image($post->id, $image);
            $this->featured_image_thumbnail($post->id, $image);
            News::find($post->id)->update(['featured_image' => $file_name]);
        }

        if (!empty($post->id)) {
            return Response::json(['status' => true,'message' => 'Data add successfully.']);
        } else {
            return Response::json(['status' => false,'message' => 'Operation failed !']);
        }
    }


    public function featured_image($id, $image) {
        $filename = $id . '.' . $image->getClientOriginalExtension();
        $location = get_featured_image_path($filename);
        // create new image with transparent background color
        $background = Image::canvas(688, 387);
        // read image file and resize it to 200x200
        $img = Image::make($image);
        // Image Height
        $height = $img->height();
        // Image Width
        $width = $img->width();
        $x = NULL;
        $y = NULL;
        if ($width > $height) {
            $y = 688;
        } else {
            $x = 387;
        }
        //Resize Image
        $img->resize($x, $y, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        // insert resized image centered into background
        $background->insert($img, 'center');
        // save
        $background->save($location);
        return $filename;
    }

    public function featured_image_thumbnail($id, $image) {
        $filename = $id . '.' . $image->getClientOriginalExtension();
        $location = get_featured_image_thumbnail_path($filename);
        // create new image with transparent background color
        $background = Image::canvas(370, 235);
        // read image file and resize it to 200x200
        $img = Image::make($image);
        // Image Height
        $height = $img->height();
        // Image Width
        $width = $img->width();
        $x = NULL;
        $y = NULL;
        if ($width > $height) {
            $y = 370;
        } else {
            $x = 235;
        }
        //Resize Image
        $img->resize($x, $y, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        // insert resized image centered into background
        $background->insert($img, 'center');
        // save
        $background->save($location);
        return $filename;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['news'] = News::where('id', $id)->first();
        return Response::json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = News::find($id);

        if ($post->post_slug == $request->post_slug) {
            $post_slug = "required|alpha_dash|min:5|max:150";
        } else {
            $post_slug = "required|alpha_dash|min:5|max:150|unique:news";
        }

        $url = '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
        request()->validate([
            'post_date' => 'required|date',
            'post_title' => 'required|string|max:255',
            'post_slug' => $post_slug,
            'post_details' => 'required|string',
            'featured_image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:10240|dimensions:max_width=10000,max_height=60000',
            'publication_status' => 'required',
            'is_featured' => 'required',
            'meta_title' => 'required|max:250',
            'meta_keywords' => 'required|max:250',
            'meta_description' => 'required|max:400',
        ]);

        $post->post_title = $request->input('post_title');
        $post->post_slug = $request->input('post_slug');
        $post->post_date = $request->input('post_date');
        $post->publication_status = $request->input('publication_status');
        $post->is_featured = $request->input('is_featured');
        $post->post_details = Purifier::clean($request->input('post_details'));
        $post->meta_title = $request->input('meta_title');
        $post->meta_keywords = $request->input('meta_keywords');
        $post->meta_description = Purifier::clean($request->input('meta_description'));

        if ($request->hasFile('featured_image')) {
            $image = $request->file('featured_image');
            $file_name = $this->featured_image($id, $image);
            $this->featured_image_thumbnail($id, $image);
            $post->featured_image = $file_name;
        }

        $affected_row = $post->save();

        if (!empty($affected_row)) {
            return Response::json(['status' => true,'message' => 'Data add successfully.']);
        } else {
            return Response::json(['status' => false,'message' => 'Operation failed !']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
