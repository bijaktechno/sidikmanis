<?php

namespace App\Http\Controllers;

use App\Schedule;
use App\Userdetail;
use App\ScheduleUserdetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Validator;
// use Illuminate\Support\Facades\Input;
// use Illuminate\Support\Facades\Storage;

class ResultsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('result');
    }



    public function get() {
        $userdiklat = ScheduleUserdetail::with('schedule', 'userdetail')->get();

        return datatables()->of($userdiklat)
            
            ->addColumn('action', function ($userdiklat) {

                $paper = ($userdiklat->paper ? '':'disabled');

                $btn = '<a target="_blank" class="btn btn-primary btn-sm '.$paper.'" href="' . get_paper_url($userdiklat->paper) . '"> <i class="fa fa-download"></i> </a>';

                if(Auth::check()){
                $access = ($userdiklat->userdetail->user_id == Auth::user()->id ? '':'disabled');

                $btn .= ' <button class="btn btn-success btn-sm paper-button '.$access.'" data-url="' . route('getPaper', $userdiklat->id) . '" data-toggle="modal" aria-pressed="false" data-target="#modalpopup" ><i class="fa fa-upload"></i></button>';
                }

                return $btn;
            })
            ->addColumn('created_at', function ($userdiklat) {
                return $userdiklat->schedule->created_at;
            })
            ->editColumn('created_at', '{{ date("d F Y", strtotime($created_at)) }}')
            ->addColumn('updated_at', function ($userdiklat) {
                return $userdiklat->schedule->updated_at;
            })
            ->editColumn('updated_at', '{{ date("d F Y", strtotime($updated_at)) }}')
            ->addColumn('name', function ($userdiklat) {
                return $userdiklat->userdetail->name;
            })
            ->addColumn('nip', function ($userdiklat) {
                return $userdiklat->userdetail->nip;
            })
            ->addColumn('gol', function ($userdiklat) {
                return $userdiklat->userdetail->gol;
            })
            ->addColumn('position', function ($userdiklat) {
                return $userdiklat->userdetail->position;
            })
            ->addColumn('diklat_name', function ($userdiklat) {
                return $userdiklat->schedule->diklat_name;
            })
            ->addColumn('organizer', function ($userdiklat) {
                return $userdiklat->schedule->organizer;
            })
            ->addColumn('diklat_date', function ($userdiklat) {
                return $userdiklat->schedule->diklat_date;
            })
            ->editColumn('diklat_date', '{{ date("d F Y", strtotime($diklat_date)) }}')
            ->addColumn('funding_type', function ($userdiklat) {
                return $userdiklat->schedule->funding_type;
            })
            ->rawColumns(['action', 'name','diklat_name', 'nip', 'diklat_date', 'funding_type', 'funding_type'])
            ->setRowId('id')
            ->make(true);
    }

    public function paper($id)
    {
        $data['userdiklat'] = ScheduleUserdetail::where('id', $id)->first();

        return Response::json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userdiklat = ScheduleUserdetail::find($id);

        $validator = $validator = Validator::make($request->all(), [
            'paper_title'       => 'required',
            'paper'          => 'nullable|mimes:pdf|max:5000',
        ], [
            'paper_title.required'  => 'Judul Paper harus diisi.',
        ]);

        if ($validator->passes()) {

            $userdiklat->paper_title = $request->get('paper_title');
            $affected_row = $userdiklat->save();

            if ($request->hasFile('paper')) {
                $file = $request->file('paper');
                // $file = Input::file('file');
                $file_name = $this->file_store($id, $file);
                ScheduleUserdetail::find($id)->update(['paper' => $file_name]);
            }
            
            if (!empty($affected_row)) {
                $request->session()->flash('message', 'Data update successfully.');
            } else {
                $request->session()->flash('exception', 'Operation failed !');
            }

            return Response::json(['status' => true,'message' => 'Data update successfully.']);
        }

        return Response::json(['errors' => $validator->errors()]);
    }


    public function file_store($id, $file){

        $filename = $id . '.' . $file->getClientOriginalExtension();
        $location = get_paper_url_path();

        $file->move($location, $filename);

        return $filename;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
