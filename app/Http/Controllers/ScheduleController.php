<?php

namespace App\Http\Controllers;


use App\Schedule;
use App\Userdetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $userdetails = Userdetail::whereNotIn('id', [1])->get(['id', 'name']);
        return view('schedule', compact('userdetails'));
    }

    public function get() {
        $schedule = Schedule::all();

        return datatables()->of($schedule)
            ->editColumn('created_at', '{{ date("d F Y", strtotime($created_at)) }}')
            ->editColumn('updated_at', '{{ date("d F Y", strtotime($updated_at)) }}')
            ->editColumn('diklat_date', '{{ date("d F Y", strtotime($diklat_date)) }}')
            ->addColumn('pdf_file', function ($schedule) {
                return '<a target="_blank" class="btn btn-primary btn-sm" href="' . get_pdf_url($schedule->file) . '"> <i class="fa fa-download"></i> </a>';})
            ->addColumn('username', function ($schedule) {
                return '<a class="user-view-button" role="button" tabindex="0" data-id="' . $schedule->user->id . '">' . $schedule->user->username . '</a>';})
            ->addColumn('action', function ($schedule) {return '<button class="btn btn-primary btn-sm edit-button" data-url="' . route('agenda-diklat.edit', $schedule->id) . '" data-toggle="modal" aria-pressed="false" data-target="#modalpopup" ><i class="fa fa-edit"></i></button>
                    <button class="btn btn-danger btn-sm delete-button" data-id="' . $schedule->id . '"data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash"></i></button>';})
                /*return '<button class="btn btn-info btn-sm view-button" data-id="' . $schedule->id . '" data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye"></i></button>
                    <button class="btn btn-primary btn-sm edit-button" data-url="' . route('agenda-diklat.edit', $schedule->id) . '" data-toggle="modal" aria-pressed="false" data-target="#modalpopup" ><i class="fa fa-edit"></i></button>
                    <button class="btn btn-danger btn-sm delete-button" data-id="' . $schedule->id . '"data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash"></i></button>';})*/
            ->rawColumns(['pdf_file', 'username', 'action'])
            ->setRowId('id')
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $validator = Validator::make($request->all(), [
            'diklat_name'       => 'required',
            'organizer'         => 'required',
            'participant_qty'   => 'required',
            'pns_qty'           => 'required',
            'diklat_date'       => 'required|date',
            'funding_type'      => 'required',
            'contact_person'    => 'required',
            'pdf_file'          => 'required|mimes:pdf|max:5000',
        ], [
            'diklat_name.required'  => 'Nama diklat harus diisi.',
            'organizer.required'    => 'Nama Penyelenggara diklat harus diisi.',
        ]);

        if ($validator->passes()) {
            $schedule = Schedule::create([
                'user_id'           => Auth::user()->id,
                'diklat_name'       => $request->input('diklat_name'),
                'organizer'         => $request->input('organizer'),
                'participant_qty'   => $request->input('participant_qty'),
                'pns_qty'           => $request->input('pns_qty'),
                'diklat_date'       => $request->input('diklat_date'),
                'funding_type'      => $request->input('funding_type'),
                'contact_person'    => $request->input('contact_person'),
            ]);

            $schedule_id = $schedule->id;


            if (isset($request->user_diklats)) {
                $schedule->userdetails()->sync($request->user_diklats, false);
            } else {
                $schedule->userdetails()->sync(array());
            }

            if ($request->hasFile('pdf_file')) {
                $file = $request->file('pdf_file');
                // $file = Input::file('file');
                $file_name = $this->file_store($schedule_id, $file);
                Schedule::find($schedule_id)->update(['file' => $file_name]);
            }
            

            if (!empty($schedule_id)) {
                $request->session()->flash('message', 'Data add successfully.');
            } else {
                $request->session()->flash('exception', 'Operation failed !');
            }

            return Response::json(['status' => true,'message' => 'Data add successfully.']);
        }

        return Response::json(['errors' => $validator->errors()]);
    }

    public function file_store($id, $file){

        // dd($file);
        $filename = $id . '.' . $file->getClientOriginalExtension();
        $location = get_pdf_url_path();

        $file->move($location, $filename);

        // Storage::put($location, );

        return $filename;
/*

        $uniqueFileName = uniqid() . $request->get('upload_file')->getClientOriginalName() . '.' . $request->get('upload_file')->getClientOriginalExtension());

        $request->get('upload_file')->move(public_path('files') . $uniqueFileName);

        return redirect()->back()->with('success', 'File uploaded successfully.');*/

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['schedule'] = Schedule::where('id', $id)->first();
        $data['userschedule'] = $data['schedule']->userdetails()->pluck('userdetail_id');

        return Response::json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);exit;
        $schedule = Schedule::find($id);

        $validator = $validator = Validator::make($request->all(), [
            'diklat_name'       => 'required',
            'organizer'         => 'required',
            'participant_qty'   => 'required',
            'pns_qty'           => 'required',
            'diklat_date'       => 'required|date',
            'funding_type'      => 'required',
            'contact_person'    => 'required',
            'pdf_file'          => 'nullable|mimes:pdf|max:5000',
        ], [
            'diklat_name.required'  => 'Nama diklat harus diisi.',
            'organizer.required'    => 'Nama Penyelenggara diklat harus diisi.',
        ]);

        if ($validator->passes()) {

            $schedule->diklat_name = $request->get('diklat_name');
            $schedule->organizer = $request->get('organizer');
            $schedule->participant_qty = $request->get('participant_qty');
            $schedule->pns_qty = $request->get('pns_qty');
            $schedule->diklat_date = $request->get('diklat_date');
            $schedule->funding_type = $request->get('funding_type');
            $schedule->contact_person = $request->get('contact_person');
            $affected_row = $schedule->save();

            if ($request->hasFile('pdf_file')) {
                $file = $request->file('pdf_file');
                // $file = Input::file('file');
                $file_name = $this->file_store($id, $file);
                Schedule::find($id)->update(['file' => $file_name]);
            }
            
            if (isset($request->user_diklats)) {
                $schedule->userdetails()->sync($request->user_diklats);
            } else {
                $schedule->userdetails()->sync(array());
            }

            if (!empty($affected_row)) {
                $request->session()->flash('message', 'Data update successfully.');
            } else {
                $request->session()->flash('exception', 'Operation failed !');
            }

            return Response::json(['status' => true,'message' => 'Data update successfully.']);
        }

        return Response::json(['errors' => $validator->errors()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Schedule::find($id);
        if (count($data)) {
            $data->delete();
            return redirect()->back()->with('message', 'Agenda berhasil di delete.');
        } else {
            return redirect()->back()->with('exception', 'Agenda tidak ada !');
        }
    }
}
