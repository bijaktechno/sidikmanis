<?php

namespace App\Http\Controllers;

use App\Requirement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;

class RequirementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requirement = Requirement::paginate(10);

        // dd($requirement);
        return view('requirement', compact('requirement'));
        //
    }


    public function get() {
        $requirement = Requirement::all();

        return datatables()->of($requirement)
            ->editColumn('created_at', '{{ date("d F Y", strtotime($created_at)) }}')
            ->editColumn('updated_at', '{{ date("d F Y", strtotime($updated_at)) }}')
            ->addColumn('username', function ($requirement) {
                return '<a class="user-view-button" role="button" tabindex="0" data-id="' . $requirement->user->id . '">' . $requirement->user->username . '</a>';})
            ->addColumn('action', function ($requirement) {
                return '<button class="btn btn-primary btn-sm edit-button" data-url="' . route('kebutuhan-diklat.edit', $requirement->id) . '" data-toggle="modal" aria-pressed="false" data-target="#modalpopup" ><i class="fa fa-edit"></i></button>
                    <button class="btn btn-danger btn-sm delete-button" data-id="' . $requirement->id . '"data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash"></i></button>';})

                /*return '<button class="btn btn-info btn-sm view-button" data-id="' . $requirement->id . '" data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye"></i></button>
                    <button class="btn btn-primary btn-sm edit-button" data-url="' . route('kebutuhan-diklat.edit', $requirement->id) . '" data-toggle="modal" aria-pressed="false" data-target="#modalpopup" ><i class="fa fa-edit"></i></button>
                    <button class="btn btn-danger btn-sm delete-button" data-id="' . $requirement->id . '"data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash"></i></button>';})*/
            ->rawColumns(['username', 'action'])
            ->setRowId('id')
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $validator = Validator::make($request->all(), [
            'scope' => 'required',
            'diklat_type' => 'required',
        ], [
            'scope.required' => 'Kelompok bidang harus diisi.',
            'diklat_type.required' => 'Jenis diklat harus diisi.',
        ]);

        if ($validator->passes()) {
            $requirement = Requirement::create([
                'user_id' => Auth::user()->id,
                'scope' => $request->input('scope'),
                'diklat_type' => $request->input('diklat_type'),
            ]);
            $requirement_id = $requirement->id;

            if (!empty($requirement_id)) {
                $request->session()->flash('message', 'Category add successfully.');
            } else {
                $request->session()->flash('exception', 'Operation failed !');
            }

            return Response::json(['status' => true,'message' => 'Data add successfully.']);
        }

        return Response::json(['errors' => $validator->errors()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['post'] = Requirement::where('id', $id)->first();

        return Response::json($data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requirement = Requirement::find($id);

        $validator = $validator = Validator::make($request->all(), [
            'scope' => 'required',
            'diklat_type' => 'required',
        ], [
            'scope.required' => 'Kelompok bidang harus diisi.',
            'diklat_type.required' => 'Jenis diklat harus diisi.',
        ]);

        if ($validator->passes()) {
            $requirement->scope = $request->get('scope');
            $requirement->diklat_type = $request->get('diklat_type');
            $affected_row = $requirement->save();

            if (!empty($affected_row)) {
                $request->session()->flash('message', 'Category update successfully.');
            } else {
                $request->session()->flash('exception', 'Operation failed !');
            }
            return Response::json(['status' => true,'message' => 'Data update successfully.']);
        }
        return Response::json(['errors' => $validator->errors()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Requirement::find($id);
        if (count($post)) {
            $post->delete();
            return redirect()->back()->with('message', 'Kebutuhan diklat berhasil di delete.');
        } else {
            return redirect()->back()->with('exception', 'Kebutuhan diklat tidak ada !');
        }
    }
}
