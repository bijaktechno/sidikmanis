<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    
    protected function authenticated($request, $user) {
        $data = array(
            'status' => true, 
            'message' => 'selamat datang '.$user->username
        );
        
        return $data;
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    
    public function logout(Request $request) {
        if (isAdmin()) {
            $this->guard()->logout();
            $request->session()->invalidate();
            return redirect('/');
        } else {
            $this->guard()->logout();
            $request->session()->invalidate();
            return redirect('/');
        }
    }
}
