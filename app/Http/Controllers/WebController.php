<?php

namespace App\Http\Controllers;

use App\Setting;
use App\News;
use Illuminate\Http\Request;

class WebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$setting = array(
            'meta_title' => 'SIDIKMANIS',
            'meta_keywords' => 'SIDIKMANIS',
            'meta_description' => 'SIDIKMANIS',
            'website_title' => 'SIDIKMANIS',
            'copyright' => '© 2018 SIDIKMANIS. All Rights Reserved'
        );
        $setting = (object)$setting;*/
       
        $news = News::where('publication_status', 1)->orderBy('post_date', 'desc')->limit(3)->get();
        
        $setting = Setting::first(['meta_title', 'meta_keywords', 'meta_description']);
        // dd($news);

        return view('home', compact('setting', 'news'));
    }

    public function contact(){

        $setting = Setting::first();
        return view('contact', compact('setting'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
