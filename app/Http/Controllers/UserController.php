<?php

namespace App\Http\Controllers;

use App\User;
use App\Userdetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function __construct() {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user');
    }

    public function get() {
        $userdetail = Userdetail::whereNotIn('id', [1])->get();

        return datatables()->of($userdetail)
            ->editColumn('created_at', '{{ date("d F Y", strtotime($created_at)) }}')
            ->editColumn('updated_at', '{{ date("d F Y", strtotime($updated_at)) }}')
            ->addColumn('action', function ($userdetail) {return '<button class="btn btn-primary btn-sm edit-button" data-url="' . route('user-diklat.edit', $userdetail->id) . '" data-toggle="modal" aria-pressed="false" data-target="#modalpopup" ><i class="fa fa-edit"></i></button>
                    <button class="btn btn-danger btn-sm delete-button" data-id="' . $userdetail->id . '"data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash"></i></button>';})
            ->rawColumns(['action'])
            ->setRowId('id')
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'nip' => 'required|string|unique:userdetails',
            'gol' => 'required|string',
            'position' => 'required|string',
            'username' => 'required|alpha_dash|max:100|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ], [
            'nip.unique'  => 'NIP sudah ada.',
            'username.unique'  => 'Username sudah ada.',
            'email.unique'    => 'Email sudah ada.',
        ]);

        if ($validator->passes()) {

            $user = User::create([
                'email'     => $request->input('email'),
                'password'  => bcrypt($request->input('password')),
                'group_id'  => 2,
                'username'  => $request->input('username'),
            ]);


            $userdetail = Userdetail::create([
                'user_id'   => $user->id,
                'name'      => $request->input('name'),
                'nip'       => $request->input('nip'),
                'gol'       => $request->input('gol'),
                'position'  => $request->input('position'),
            ]);

            $user_id = $user->id;

            if (!empty($user_id)) {
                $request->session()->flash('message', 'User add successfully.');
            } else {
                $request->session()->flash('exception', 'Operation failed !');
            }

            return Response::json(['status' => true,'message' => 'Data add successfully.']);
        }

        return Response::json(['errors' => $validator->errors()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['userdetail'] = Userdetail::where('id', $id)->first();

        return Response::json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userdetail = Userdetail::find($id);

        if ($userdetail->nip == $request->input('nip')) {
            $nip = 'required|string';
        } else {
            $nip = 'required|string|unique:userdetails';
        }

        $validator = $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'nip' => $nip,
            'gol' => 'required|string',
            'position' => 'required|string',
        ], [
            'nip.unique'  => 'NIP sudah ada.',
        ]);

        if ($validator->passes()) {

            $userdetail->name = $request->get('name');
            $userdetail->nip = $request->get('nip');
            $userdetail->gol = $request->get('gol');
            $userdetail->position = $request->get('position');
            $affected_row = $userdetail->save();

            if (!empty($affected_row)) {
                $request->session()->flash('message', 'User update successfully.');
            } else {
                $request->session()->flash('exception', 'Operation failed !');
            }

            return Response::json(['status' => true,'message' => 'Data update successfully.']);
        }

        return Response::json(['errors' => $validator->errors()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Userdetail::find($id);
        if (count($data)) {
            $data->delete();
            return redirect()->back()->with('message', 'User berhasil di delete.');
        } else {
            return redirect()->back()->with('exception', 'User tidak ada !');
        }
    }
}
