<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
	protected $fillable = [
		'user_id', 'post_date', 'post_title', 'post_slug', 'post_details', 'featured_image', 'publication_status', 'is_featured', 'view_count', 'meta_title', 'meta_keywords', 'meta_description',
	];

	public function user() {
		return $this->belongsTo(User::class);
	}
	
	public function tags() {
		return $this->belongsToMany(Tag::class);
	}

	public function comment() {
		return $this->hasMany(Comment::class);
	}
}
