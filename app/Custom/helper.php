<?php

function get_pdf_url($var = null) {
	if ($var != null) {
		return asset('public/file/' . $var . '');
	} else {
		return asset('public/file/');
	}
}


function get_pdf_url_path($var = null) {
	if ($var != null) {
		return public_path('file/' . $var . '');
	} else {
		return public_path('file/');
	}
}


function get_paper_url($var = null) {
	if ($var != null) {
		return asset('public/paper/' . $var . '');
	} else {
		return asset('public/paper/');
	}
}


function get_paper_url_path($var = null) {
	if ($var != null) {
		return public_path('paper/' . $var . '');
	} else {
		return public_path('paper/');
	}
}

function get_featured_image_url($var = null) {
	if ($var != null) {
		return asset('public/images/news/' . $var . '');
	} else {
		return asset('public/images/news/');
	}
}

function get_featured_image_path($var = null) {
	if ($var != null) {
		return public_path('images/news/' . $var . '');
	} else {
		return public_path('images/news/');
	}
}

function get_featured_image_thumbnail_url($var = null) {
	if ($var != null) {
		return asset('public/images/news/thumbnail/' . $var . '');
	} else {
		return asset('public/images/news/thumbnail/');
	}
}


function get_featured_image_thumbnail_path($var = null) {
	if ($var != null) {
		return public_path('images/news/thumbnail/' . $var . '');
	} else {
		return public_path('images/news/thumbnail/');
	}
}

function isAdmin() {
	$user = Auth::user();
	// dd($user);
	if ($user && $user->group_id == 1) {
		return true;
	} else {
		return false;
	}
}
