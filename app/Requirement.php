<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requirement extends Model
{
    protected $fillable = [
        'user_id', 'scope', 'diklat_type'
    ];

	public function user() {
		return $this->belongsTo(User::class);
	}
}
