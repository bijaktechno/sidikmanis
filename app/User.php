<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'group_id', 'username'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function userdetail() {
        return $this->hasOne(Userdetail::class);
    }

    public function group() {
        return $this->belongsTo(Group::class);
    }
    
    public function requirement() {
        return $this->hasMany(Requirement::class);
    }
    
    public function schedule() {
        return $this->hasMany(Schedule::class);
    }

    public function news() {
        return $this->hasMany(News::class);
    }
}
