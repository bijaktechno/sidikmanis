<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleUserdetail extends Model
{
	protected $table = 'schedule_userdetail';

    protected $fillable = [
        'userdetail_id', 'schedule_id', 'paper_title', 'paper'
    ];

    public function userdetail() {
        return $this->belongsTo(Userdetail::class);
    }

    public function schedule() {
        return $this->belongsTo(Schedule::class);
    }
}
